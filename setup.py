import setuptools
import os
from setuptools import setup, find_packages


setup(
    name='checklest_e2e_artifact',
    version="1.0.0",
    author='PDA',
    author_email='essamx.zaroura@intel.com',
    description='Scrapping Producer Template',
    packages=find_packages(exclude=['tests']),
    url='https://gitlab.devtools.intel.com/PDA/PEARL/checklist-e2e-tests/',
    long_description='checklest_e2e_artifact checklest_e2e_artifact'
)