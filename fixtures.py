
from selenium import webdriver
from web_source.web import Web
import os.path

def browser_chrome(context, timeout=30, **kwargs):
    browser = webdriver.Chrome(executable_path=os.path.join(os.getcwd(), "bin", "chromedriver.exe"))
    web = Web(browser)
    context.web = web
    yield context.web
    browser.quit()
