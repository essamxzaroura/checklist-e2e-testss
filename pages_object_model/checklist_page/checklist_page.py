# from element import BasePageElement
# from navigation_page_locators import NavigationPageLocators
import time
import random
import unittest
from telnetlib import EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from entities.drops_entity.drop_checklist_entity import DropChecklists
from pages_object_model.base_page.base_page_web import BasePageWeb
from pages_object_model.navigation_page.navigation_page import NavigationPage
from pages_object_model.drops_page.drops_page import DropsPage
from pages_object_model.navigation_page.navigation_page_locators import NavigationPageLocators
# from pages_object_model.common_page.common_page import CommonPage
from pages_object_model.drops_page.drops_page_locators import DropsPageLocators
from pages_object_model.approvers_page.approvers_page_locators import ApproversPageLocators
from pages_object_model.checklist_page.checklist_page_locators import ChecklistPageLocators
from utils import connectToDB
from selenium.webdriver import ActionChains
from datetime import datetime
import re

class ChecklistPage(BasePageWeb):
    #to create checklist class
    def __init__(self, driver):
        self.Configuration = "" #if like ipConfig Name
        self.dropname = ""
        self.ipFamily = ""
        self.ipConfigName = ""
        self.checklistID = ""
        self.query_result_copy_from = ""
        self.resultDictionary = {}
        self.changedRules2_result = {}
        self.changedRules1_result = {}
        self.changedRules2 = {}
        self.changedRules1 = {}
        self.db = ""
        self.len_cl_copy_from = 0
        self.grade = 0
        self.driver=driver

        self.checklistName_label=''
        self.checklist_versionName_label=''
        self.ipConfigurationName_label = ''
        self.checklist_revision_milestoneid_label = ''

    def update_page_details(self):
        time.sleep(1)
        if (self.wait_to_be_displayed(*ChecklistPageLocators.ipConfigurationName_label)):
            self.checklistName_label = self.find_element(*ChecklistPageLocators.checklistName_label).text
            self.checklist_versionName_label = self.find_element(*ChecklistPageLocators.checklist_versionName_label).text
            self.ipConfigurationName_label = self.find_element(*ChecklistPageLocators.ipConfigurationName_label).text
            self.checklist_revision_label = self.find_element(*ChecklistPageLocators.checklist_revision_milestoneid_label).text.split("\\")[0]
            self.checklist_milestoneid_label = self.find_element(*ChecklistPageLocators.checklist_revision_milestoneid_label).text.split("\\")[1]

    def click_on_CopyFrom__button(self):
        self.wait_and_click_on_element(*ChecklistPageLocators.CopyFrom)

    def click_on_CopyTo__button(self):
        if(self.wait_for_element_be_invisible(*ChecklistPageLocators.notification_result_saved)):
            self.wait_and_click_on_element(*ChecklistPageLocators.CopyTo)

    def get_ipConfigId_label(self):
        try:
            return self.find_element(*ChecklistPageLocators.ipConfigurationName_label).text
        except NoSuchElementException:
            return False

    def click_on_close_copy_checklist_window_button(self):
        self.wait_and_click_on_element(*ChecklistPageLocators.close_copy_checklist_window_button)

    def select_all_checklists_from_DB(self,env):
        self.db = connectToDB.SQLQuiries(env)
        query_result_checklists_info = connectToDB.SQLQuiries.select_all_checklists_from_vwChecklistStatus(self.db)
        query_result_checklists_column_name = list( query_result_checklists_info[0].cursor_description)
        query_result_checklists_info = self.Parse_query_result(query_result_checklists_info,0)
        self.Create_Query_Dict (query_result_checklists_column_name,query_result_checklists_info,self.resultDictionary,0)
        return self.resultDictionary


    def select_all_checklists_from_DB_app(self,env):
        self.db = connectToDB.SQLQuiries(env)
        self.db_checklist_result = connectToDB.SQLQuiries.select_all_checklists_from_vwChecklistStatus(self.db)
        return self.db_checklist_result

    def select_2_checklists_from_DB_to_copy(self,env):
        self.db = connectToDB.SQLQuiries(env)
        IPFamily_ChecklistID_DropName = self.get_IpFamily_checklistID_DropName_To_copyFromTo(env,self.db)
        query_result_checklists_info = connectToDB.SQLQuiries.get_relevant_checklists_to_copy_from_to(self.db,IPFamily_ChecklistID_DropName[0],int(IPFamily_ChecklistID_DropName[1]),IPFamily_ChecklistID_DropName[2])# two_checklists_to_copy
        query_result_checklists_column_name1 = list( query_result_checklists_info[0].cursor_description) #columns names - row 0
        query_result_checklists_column_name2 = list( query_result_checklists_info[1].cursor_description) #columns names - row 1
        query_result_checklists_info1 = self.Parse_query_result(query_result_checklists_info,0) #columns values - row 0
        query_result_checklists_info2 = self.Parse_query_result(query_result_checklists_info,1) #columns values - row 1
        self.Create_Query_Dict(query_result_checklists_column_name1,query_result_checklists_info1,self.resultDictionary,0)
        self.Create_Query_Dict(query_result_checklists_column_name2,query_result_checklists_info2,self.resultDictionary,1)
        return self.resultDictionary

    def Create_Query_Dict(self,query_columns_names,query_columns_values,dictionary,rowIndex):# creates dictionary from query result, column name+row number as key and column value as value
        for (elemName,elemValue) in zip(query_columns_names,query_columns_values):
            dictionary[elemName[0]+str(rowIndex)] = elemValue
        return dictionary

    def get_IpFamily_checklistID_DropName_To_copyFromTo(self,env,conn):#gets_IpFamily,checklistID,DropName To get 2 relevant checklists to perform CopyTo/Copy From
        query_result_checklists_info = connectToDB.SQLQuiries.copy_from_to_get_relevant_data_for_checklist(conn)
        result = self.Parse_query_result(query_result_checklists_info,0)
        return result

    def click_on_checklist_to_copy_from_to(self,milestoneid):
        if(self.wait_to_be_displayed(*ChecklistPageLocators.Milestones_of_checklists_to_copy_from)):
            checklists_to_copy_from = self.driver.find_elements(*ChecklistPageLocators.Milestones_of_checklists_to_copy_from)
            for elem in checklists_to_copy_from:
                if(elem.text == milestoneid):
                    elem.click()

    def click_on_copy_all_rules(self):
        self.wait_and_click_on_element (*ChecklistPageLocators.copy_all_rules_radioButton)

    def click_on_copy_rules_with_approval_req(self):
        self.wait_and_click_on_element (*ChecklistPageLocators.copy_rules_with_approval_req_radiobutton)

    def click_on_waiver_cb(self):
        self.wait_and_click_on_element(*ChecklistPageLocators.waived_rules_checkbox)

    def click_on_na_cb(self):
        self.wait_and_click_on_element(*ChecklistPageLocators.na_rules_checkbox )

    def click_on_override_cb(self):
        self.wait_and_click_on_element(*ChecklistPageLocators.override_rules_checkbox )

    def click_on_copy_button(self):
        self.wait_and_click_on_element(*ChecklistPageLocators.copy_button)

    def click_on_cancel_button(self):
        self.wait_and_click_on_element(*ChecklistPageLocators.cancel_button)

    def click_on_go_to_destination_cl_link(self):
        self.wait_and_click_on_element(*ChecklistPageLocators.GoToDestinationChecklist_link)

    def get_all_responses_dropdowns(self):
        if(self.wait_to_be_displayed (*ChecklistPageLocators.response_dropdowns)):
            return self.driver.find_elements(*ChecklistPageLocators.response_dropdowns)

    def click_on_submit_button(self):
        self.wait_and_click_on_element (*ChecklistPageLocators.submit_button)

    def click_on_save_justification_button(self):
        if (self.wait_and_click_on_element(*ChecklistPageLocators.juctification_checkbox)):
            self.wait_and_click_on_element(*ChecklistPageLocators.save_juctification_button)

    def click_on_revoke_button(self):
        if (self.get_revoke_button()):
            self.wait_and_click_on_element(*ChecklistPageLocators.revoke_request_button)

    def click_on_Copy_rules_approvals_radioButton(self):
        if(self.wait_to_be_displayed(*ChecklistPageLocators.copy_rules_with_approval_req_radiobutton)):
            elem = self.driver.find_element(*ChecklistPageLocators.copy_rules_with_approval_req_radiobutton)
            self.driver.execute_script("arguments[0].click();", elem)

    def get_grade(self):
        if (self.wait_for_element_be_invisible ( *ChecklistPageLocators.notification_result_saved )):
            return int(self.driver.find_element(*ChecklistPageLocators.grade).text.replace('%',""))

    def get_rules_names(self):
        time.sleep(1)
        if(self.wait_to_be_displayed(By.XPATH,ChecklistPageLocators.rules_names)):
            return self.driver.find_elements_by_xpath(ChecklistPageLocators.rules_names)

    def set_grade_to_actual(self):
          self.grade = int(self.driver.find_element(*ChecklistPageLocators.grade).text.replace('%', ""))

    def set_text_into_justification_window(self):
        if(self.wait_to_be_displayed(*ChecklistPageLocators.justification_window)):
            # self.driver.find_element(*ChecklistPageLocators.justification_window).send_keys("justification test " + datetime.now().strftime("%m/%d/%Y, %H:%M:%S"))
            self.driver.find_element(*ChecklistPageLocators.justification_window).send_keys(*ApproversPageLocators.justification_text)

    def update_response(self,response,index):
        index = int(index)
        if(self.wait_for_element_be_invisible(*ChecklistPageLocators.notification_result_saved)):
            self.driver.execute_script("arguments[0].click();",self.get_all_responses_dropdowns()[index])
            ro = self.driver.find_elements(*ChecklistPageLocators.responses_variants)
            for elem in ro:
                if(elem.text==response):
                    actions = ActionChains(self.driver)
                    actions.move_to_element(elem).perform()
                    time.sleep(0.5)
                    elem.click()
                    time.sleep(0.5)
                    if (response == "No Response"):
                        self.click_on_revoke_button()
                        break
                    if((response == "Waiver" or response == "NA") or (response == "Passed" and self.get_zircon_responses())):
                        if( self.get_justification_window()):
                            self.set_text_into_justification_window()
                            self.click_on_save_justification_button()

    def get_approval_status(self,index):
        if (index == 'first'):
            index = 0
        if (index == 'last'):
            index = 1
            # last_res = self.get_all_responses_dropdowns()
            # index = len(last_res) - 1
        if (self.wait_to_be_displayed (By.XPATH,"//*[@data-name='checklist-table-approval-process']")):
            approval_process_response = self.driver.find_elements(By.XPATH,"//*[@data-name='checklist-table-approval-process']")[index].text
            if(approval_process_response != 'In Process'):
                approval_process_response=approval_process_response[approval_process_response.index('\n')+1:len(approval_process_response)]
            return approval_process_response

    def get_justification_window(self):
        try:
            return self.driver.find_element(*ChecklistPageLocators.justification_window)
        except NoSuchElementException:
            return False

    def get_zircon_responses(self):
        try:
            return self.driver.find_element (*ChecklistPageLocators.response_options_zircon)
        except NoSuchElementException:
            return False
                          
    def get_revoke_button(self):
        try:
            if(self.wait_to_be_displayed(*ChecklistPageLocators.revoke_request_button)):
                return self.driver.find_element(*ChecklistPageLocators.revoke_request_button)
        except NoSuchElementException:
            return False

    def get_copied_rules(self,index):
         if(index == 'first'):
             index = 0
         if(index == 'last'):
             last_res = self.get_all_responses_dropdowns()
             index = len(last_res) - 1
         else:
             index=int(index)
         if(self.wait_to_be_displayed(By.XPATH,"//span[@class='cl-row-description']")):
            key = self.driver.find_elements_by_xpath("//span[@class='cl-row-description']")[index].text
            if(self.wait_to_be_displayed(By.XPATH, "//input[@data-name='checklist-table-response']//.." ) and self.wait_for_element_be_invisible(*ChecklistPageLocators.notification_result_saved)):
                self.changedRules2[key] = self.driver.find_elements_by_xpath("//input[@data-name='checklist-table-response']//..")[index].text

    def get_copied_response_rules(self,ipconfigName,index):
        index=int(index)
        if(self.wait_to_be_displayed(*ChecklistPageLocators.item_row_rule_description)):
            key = self.driver.find_elements(*ChecklistPageLocators.item_row_rule_description)[index].text
            if(self.wait_to_be_displayed(*ChecklistPageLocators.checklist_table_respons)and ipconfigName=='2'):
                self.changedRules2_result[key] = self.driver.find_elements(*ChecklistPageLocators.checklist_table_respons)[index].text
            if(self.wait_to_be_displayed(*ChecklistPageLocators.checklist_table_respons)and ipconfigName=='1'):
                self.changedRules1_result[key] = self.driver.find_elements(*ChecklistPageLocators.checklist_table_respons)[index].text

    def open_checklist_by_Ipconfigname_and_checklistIName(self,IpConfigname,checklistName,dropName,milestoneID, navigation_page,drop_page,common_page):
        common_page.click_on_HOME__button()
        navigation_page.click_on_select_different_ip_button()
        navigation_page.select_your_ip_by_group(None,None,None,IpConfigname)
        navigation_page.click_on_select_button()
        if not(drop_page.is_dropname_in_droppage(dropName)):
            drop_page.select_Milestone_Range_Drop(dropName)
        drop_page.clickOnRelevantChecklist(checklistName, str(milestoneID))

    def Copy_From(self,ipconfigName,dropName,milestoneid):
        self.click_on_CopyFrom__button()
        self.set_checklist_to_copy_from_or_to(ipconfigName,dropName)
        self.click_on_checklist_to_copy_from_to(milestoneid)
        self.click_on_copy_button()
        self.click_on_close_copy_checklist_window_button()

    def Copy_From_approval_requests_only(self,ipconfigName,dropName,milestoneid):
        self.click_on_CopyFrom__button()
        self.set_checklist_to_copy_from_or_to(ipconfigName,dropName)
        self.click_on_checklist_to_copy_from_to(milestoneid)
        self.click_on_Copy_rules_approvals_radioButton()
        self.click_on_override_cb()
        self.click_on_copy_button()
        self.click_on_close_copy_checklist_window_button()

    def Copy_To(self,ipconfigName,dropName,milestoneid):
        self.click_on_CopyTo__button()
        self.set_checklist_to_copy_from_or_to(ipconfigName,dropName)
        self.click_on_checklist_to_copy_from_to(milestoneid)
        self.click_on_copy_button()
        self.click_on_go_to_destination_cl_link()    

    def Copy_To_approval_requests_only(self,ipconfigName,dropName,milestoneid):
        self.click_on_CopyTo__button()
        self.set_checklist_to_copy_from_or_to(ipconfigName,dropName)
        self.click_on_Copy_rules_approvals_radioButton()
        self.click_on_checklist_to_copy_from_to(milestoneid)
        self.click_on_copy_button()
        self.click_on_go_to_destination_cl_link()    

    def set_checklist_to_copy_from_or_to(self,ipConfigName,drop):
        if(self.wait_to_be_displayed(*ChecklistPageLocators.Select_ip_configuration_arrow_drop_down)):
            self.driver.find_element(*ChecklistPageLocators.Select_ip_configuration_arrow_drop_down).send_keys(ipConfigName)
            self.driver.find_element(*ChecklistPageLocators.Select_Milestone_arrow_drop_down_after_choice).click()
            self.driver.find_element(*ChecklistPageLocators.Select_ip_configuration_or_milestone_arrow_drop_down_option).click()
            self.driver.find_element(*ChecklistPageLocators.Select_Milestone_arrow_drop_down).send_keys(drop)
            self.driver.find_element(*ChecklistPageLocators.Select_ip_configuration_or_milestone_arrow_drop_down_option_after_choice).click()

    def update_rule(self,response,index):
        self.update_response(response,index)
        self.click_on_submit_button()

    def Copy_Rules(self,action,IpConfigurationName,dropName,milestoneid):
        if(action == 'from'):
            self.Copy_From(IpConfigurationName,dropName,str(milestoneid))
        if (action == 'to'):
            self.Copy_To(IpConfigurationName,dropName,str(milestoneid) )

    def Copy_Requested_rules_only(self,action,IpConfigurationName,dropName,milestoneid):
        if(action == 'from'):
            self.Copy_From_approval_requests_only(IpConfigurationName,dropName,str(milestoneid))
        if (action == 'to'):
            self.Copy_To_approval_requests_only(IpConfigurationName,dropName,str(milestoneid))

    def Parse_query_result(self,query_result,index):
        list(query_result[index].cursor_description)
        query_result_checklists_info = str(query_result[index])
        pattern = r'[\']'
        regex = re.compile(pattern)
        result = regex.sub('',query_result_checklists_info).replace("(","",1).replace(")","",len(query_result_checklists_info)-1).split (",")
        query_result_checklists_info = []
        for elem in result:
            if(elem[0] == " "):
                elem = elem.replace(" ","",1)
            query_result_checklists_info.append(elem)
        return query_result_checklists_info

    def get_response_rule_by_index(self, index):
        return self.driver.find_elements(*ChecklistPageLocators.checklist_table_responses)[index].text