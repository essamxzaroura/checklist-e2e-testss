from selenium.webdriver.common.by import By


class ChecklistPageLocators(object):

    CopyFrom = (By.XPATH, "//div[@data-id = 'checklist-copy-from-btn']")
    CopyTo = (By.XPATH, "//div[@data-id = 'checklist-copy-to-btn']")   
    Select_ip_configuration_arrow_drop_down = (By.XPATH, "//input[@data-id = 'checklist-copy-ipconfiguration-dd']")
    Select_Milestone_arrow_drop_down = (By.XPATH, "//input[@data-id = 'checklist-copy-milestone-dd']")
    Select_Milestone_arrow_drop_down_after_choice = (By.XPATH, "//span[@class='v-list__tile__mask']")
    Select_ip_configuration_or_milestone_arrow_drop_down_option = (By.XPATH,"//input[@data-id = 'checklist-copy-milestone-dd']")
    Select_ip_configuration_or_milestone_arrow_drop_down_option_after_choice = (By.XPATH,"//span[@class = 'v-list__tile__mask']")
    #title of checklist page
    checklistName_label = (By.XPATH, "//div[@data-id = 'checklist-name-label']")
    checklist_versionName_label = (By.XPATH, "//*[@class = 'cl-checklist-version']")
    ipConfigurationName_label = (By.XPATH, "//div[@data-id = 'checklist-ip-name-label']")
    checklist_revision_milestoneid_label = (By.XPATH, "//div[@data-id = 'checklist-revision-milestoneid-label']")

    Grades_of_checklists_to_copy_from = (By.XPATH, "//td[@data-name = 'checklist-copy-table-grade']")
    Milestones_of_checklists_to_copy_from = (By.XPATH,"//td[@data-name = 'checklist-copy-table-milestoneID']")
    copy_all_rules_radioButton = (By.XPATH, "//div[@class = 'v-input--selection-controls__input']////input[@data-id = 'checklist-copy-all-radio-btn']")
    copy_rules_with_approval_req_radiobutton = (By.XPATH, "//div[@class = 'v-input--selection-controls__input']//input[@data-id ='checklist-copy-approval-radio-btn']")
    waived_rules_checkbox = (By.XPATH, "//input[@data-id = 'checklist-copy-approval-waived-cb']")
    na_rules_checkbox = (By.XPATH, "//input[@data-id = 'checklist-copy-approval-na-cb']")
    override_rules_checkbox = (By.XPATH,"//*[@data-id ='checklist-copy-version-approval-cb']")
    copy_button = (By.XPATH, "//div[@data-id = 'checklist-copy-copy-btn']")
    cancel_button = (By.XPATH, "//div[@data-id = 'checklist-copy-cancel-btn']")
    close_copy_checklist_window_button = (By.XPATH, "//button//div[contains(text(),'close')]")
    response_dropdowns = (By.XPATH, "//input[@data-name ='checklist-table-response']/parent::div[@class = 'v-select__selections']/following-sibling::div[@class='v-input__append-inner']")
    checklist_table_responses = (By.XPATH, "//input[@data-name='checklist-table-response']//..")

    response_options_list = (By.XPATH, "//div[@class = 'v-menu__content theme--light menuable__content__active']")
    response_options_zircon = (By.XPATH, "//div[@data-name = 'checklist-table-zircon-result']")
    submit_button = (By.XPATH, "//div[@data-id='checklist-submit-btn']")
    grade = (By.XPATH, "//div[@class='cl-checklist-grade']//span")
    GoToDestinationChecklist_link = (By.XPATH, "//div[@class='cl-message-link']")
    justification_window = (By.XPATH, "//div[@class='v-text-field__slot']//textarea")
    save_juctification_button = (By.XPATH, "//div[@data-id='modal-Save-btn']")
    juctification_checkbox = (By.XPATH, "//div[@class='v-input--selection-controls__ripple']")
    cancel_juctification_button = (By.XPATH, "//div[@data-id='modal-Cancel-btn']")
    revoke_request_button = (By.XPATH, "//div[@data-id = 'modal-Revoke-btn']")
    cancel_revoke_request_button = (By.XPATH, "//div[@data-id = 'modal-Cancel-btn']")
    rules_names = "//span[@class = 'cl-row-description']"
    responses_variants = (By.XPATH,"//div[@class='v-menu__content theme--light menuable__content__active']//div[@class='v-list__tile__title']")
    notification_result_saved = (By.XPATH,"//div[@class='notifications']")

    item_row_rule_description=(By.XPATH,"//span[@class='cl-row-description']")
    checklist_table_respons=(By.XPATH, "//input[@data-name='checklist-table-response']//..")