from telnetlib import EC
import time
import random
from selenium.common.exceptions import NoSuchElementException
from entities.drops_entity.drop_checklist_entity import DropChecklists
from pages_object_model.base_page.base_page_web import BasePageWeb
from pages_object_model.checklist_page.checklist_page import ChecklistPage
from pages_object_model.common_page.common_page_locators import CommonPageLocators
from pages_object_model.approvers_page.approvers_page_locators import ApproversPageLocators
from pages_object_model.common_page.common_page_locators import CommonPageLocators
from pages_object_model.drops_page.drops_page_locators import DropsPageLocators
from pages_object_model.navigation_page.navigation_page import NavigationPage
from utils import connectToDB


class CommonPage(BasePageWeb):
    def __init__(self, driver):
        self.driver=driver
        self.version_is_changed = False

    def click_on_HOME__button(self):
        self.wait_and_click_on_element(*CommonPageLocators.Home_button)

    def click_on_save_button(self):
        self.wait_and_click_on_element(*CommonPageLocators.saveButton)

    def click_on_settings_button(self):
        if(self.wait_to_be_displayed(*CommonPageLocators.settingsButton)):
            elem = self.find_element(*CommonPageLocators.settingsButton)
            self.driver.execute_script("arguments[0].click();",elem)

    def click_on_approvers_button(self):
        if(self.wait_to_be_displayed(*CommonPageLocators.Approvers_button)):
            elem = self.find_element(*CommonPageLocators.Approvers_button)
            self.driver.execute_script("arguments[0].click();",elem)
            time.sleep(1)

    def click_on_done_button(self):
        self.wait_and_click_on_element ( *CommonPageLocators.doneButton)
        # self.driver.find_element ( *CommonPageLocators.doneButton ).click ()

    def get_done_button(self):
        try:
            return self.driver.find_element ( *CommonPageLocators.doneButton )
        except NoSuchElementException:
            return False

    def get_If_first_time_user(self):
        try:
            if ('Which Checklists are you interested in?' in self.driver.find_element(*CommonPageLocators.preferences_window_first_time_user).text):
                return True
        except NoSuchElementException:
            return False

    def checked_checkbox_checklist_type(self, check, selectType):
        CheckBoxs = self.driver.find_elements(*CommonPageLocators.ChecklistsCheckBox)
        for elem in CheckBoxs:
            if elem.get_attribute("aria-label")==selectType and elem.get_attribute("aria-checked")==check:
                self.driver.execute_script("arguments[0].click();", elem)

    def get_unchecked_checklists(self):
        if (self.get_done_button() is False):
            self.click_on_settings_button()
        return self.driver.find_elements(*CommonPageLocators.unMarkedChecklistsCheckBox)


    def get_preferences_window(self):
        try:
            return self.driver.find_element(*CommonPageLocators.preferences_window)
        except NoSuchElementException:
            return False

    def get_preferences_window_first_time_user(self):
        try:
            return self.driver.find_element(*CommonPageLocators.preferences_window_first_time_user)
        except NoSuchElementException:
            return False

    def get_checked_checklists_names(self):
        self.click_on_settings_button()
        time.sleep(1)
        marked_cl_names = self.driver.find_elements(*CommonPageLocators.markedChecklistsCheckBox_CL_Names)
        marked_cl_names_lst = []
        if(self.wait_to_be_displayed(*CommonPageLocators.preferences_window)):
            for elem in marked_cl_names:
                marked_cl_names_lst.append(elem.text)
            return marked_cl_names_lst

    def get_owned_by_me_list(self):
        if(self.wait_to_be_displayed(*CommonPageLocators.owned_by_me_list)):
            return self.driver.find_elements(*CommonPageLocators.owned_by_me_list)

    def get_checked_checklists(self):
        if(self.get_done_button()is False):
            self.click_on_settings_button()
        return self.driver.find_elements(*CommonPageLocators.markedChecklistsCheckBox)

    def AllCheckboxesAreFull(self):
        if(self.get_preferences_window() is False) and (self.get_preferences_window_first_time_user() is False):
            self.click_on_settings_button()
        UnChecked = self.driver.find_elements(*CommonPageLocators.unMarkedChecklistsCheckBox)
        if (len(UnChecked) > 0):
            self.clean_CL_preferences(UnChecked)
        else:
            Checked = self.driver.find_elements(*CommonPageLocators.markedChecklistsCheckBox)
            self.clean_CL_preferences(Checked)
            UnChecked = self.driver.find_elements(*CommonPageLocators.unMarkedChecklistsCheckBox)
            self.clean_CL_preferences(UnChecked)

        if(self.get_done_button()):
             self.click_on_done_button()
        else:
            self.click_on_save_button()

    def checked_box_checklist_type(self,type):
        UnChecked = self.driver.find_elements(*CommonPageLocators.unMarkedChecklistsCheckBox)
        if (len(UnChecked) > 0):
            self.clean_CL_preferences(UnChecked)
            self.click_on_save_button()
        else:
            Checked = self.driver.find_elements(*CommonPageLocators.markedChecklistsCheckBox)
            self.clean_CL_preferences(Checked)
            UnChecked = self.driver.find_elements(*CommonPageLocators.unMarkedChecklistsCheckBox)
            self.clean_CL_preferences(UnChecked)
            self.click_on_save_button()

    def clean_CL_preferences(self, UnChecked):
        for elem in UnChecked:
            self.driver.execute_script("arguments[0].click();", elem)

    def EmptyAllCheckBoxesAndClickOnSave(self):
        Checked = self.driver.find_elements(*CommonPageLocators.markedChecklistsCheckBox)
        if (len(Checked) > 0):
            self.clean_CL_preferences(Checked)
            time.sleep(0.5)
            Save = self.driver.find_element(*CommonPageLocators.saveButton)
            try:
                Save.click()
            except:
                assert Save.is_displayed(), "Save is clickable"

    def ChooseRandomCL(self):
        UnChecked = self.get_unchecked_checklists()
        if (len(UnChecked) == 0):
            Checked = self.driver.find_elements(*CommonPageLocators.markedChecklistsCheckBox)
            self.clean_CL_preferences(Checked)
        UnChecked = self.driver.find_elements(*CommonPageLocators.unMarkedChecklistsCheckBox)
        iterations = random.randint(2,len(UnChecked))
        randomes = list(range(len(UnChecked)))
        count = 0
        while(count<iterations):
            index = random.randint(0, len(randomes)-1- count)
            self.driver.execute_script("arguments[0].click();", UnChecked[randomes[index]])
            temp = randomes[len(randomes)-1-count]
            randomes[len(randomes)-1-count] = randomes[index]
            randomes[index] = temp
            count = count+1
        self.Checked_after_random = self.driver.find_elements(*CommonPageLocators.markedChecklistsCheckBox_CL_Names)
        self.Checked_after_random_lst = []
        for elem in self.Checked_after_random:
            self.Checked_after_random_lst.append(elem.text)
        # for i in range(0,len( self.Checked_after_random)):
        #     self.Checked_after_random_lst.append(self.Checked_after_random[i].text)
        if(self.get_done_button()is not False):
             self.click_on_done_button()
        else:
            self.click_on_save_button()
        time.sleep(1)

    def get_owner(self,ipconfigID,env):
        self.resultDictionary = {}
        self.db = connectToDB.SQLQuiries(env)
        query_result_user_info = connectToDB.SQLQuiries.get_owner_by_ipConfigID(self.db,int(ipconfigID))
        if(len(query_result_user_info) > 0):
            query_result_checklists_column_name = list(query_result_user_info[0].cursor_description)
            result = ChecklistPage.Parse_query_result(self,query_result_user_info,0)
            ChecklistPage.Create_Query_Dict(self,query_result_checklists_column_name,result,self.resultDictionary,0)
            return self.resultDictionary
        else: return "No owner"

    def update_owner(self,env,ipconfigID,username):
        self.db = connectToDB.SQLQuiries(env)
        connectToDB.SQLQuiries.update_owner_by_ipConfigID(self.db,username,int(ipconfigID))

    def make_owner(self,env,ipconfigID,username):
        self.db = connectToDB.SQLQuiries(env)
        connectToDB.SQLQuiries.make_owner_by_ipConfigID(self.db,int(ipconfigID),username)

    def delete_owner(self,env,username,ipconfigID):
        self.db = connectToDB.SQLQuiries(env)
        connectToDB.SQLQuiries.delete_owner_by_ipConfigID_and_idsid(self.db,username,int(ipconfigID))

    def select_version(self,version=1):
        if version != 1:
            self.driver.find_element_by_xpath('{0}{1}{2}'.format(CommonPageLocators.version_radio_button,version,"\')]")).click()
        self.wait_and_click_on_element(*CommonPageLocators.assign_version_manually_button)
        self.version_is_changed=True

    def select_version_if_get_started(self):
        if (self.wait_to_be_displayed(*CommonPageLocators.assign_version_manually_button)):
            return True
        else:
            return False

    def switch_mode(self,mode):
        if (mode=='released'):
            self.wait_and_click_on_element(*CommonPageLocators.toolbar_released_toggle_btn)
        else:
            self.wait_and_click_on_element(*CommonPageLocators.toolbar_development_toggle_btn)

    def db_return_Get_started(self, env, milestoneID, checklistID):
        if self.version_is_changed:
            self.db = connectToDB.SQLQuiries(env)
            connectToDB.SQLQuiries.delete_assigned_checklist_version_get_started(self.db,milestoneID,checklistID)
            self.version_is_changed = False