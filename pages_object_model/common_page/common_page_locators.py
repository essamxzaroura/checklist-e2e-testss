from selenium.webdriver.common.by import By


class CommonPageLocators(object):

    Home_button=(By.XPATH,"//button//div[contains(text(),'Home')]")
    # Home_button = (By.XPATH,"//*[@data-id='toolbar-home-link']")
    Approvers_button = (By.XPATH,"//button[@data-id='toolbar-approver-requests-link']")
    settingsButton = (By.XPATH,"//*[@data-id='toolbar-settings-btn']")
    unMarkedChecklistsCheckBox = (By.XPATH,"//input[@type = 'checkbox' and @aria-checked='false']")
    markedChecklistsCheckBox =  (By.XPATH,"//input[@type = 'checkbox' and @aria-checked='true']")
    saveButton = (By.XPATH, "//*[@data-id='modal-Save-btn']")
    ChecklistsCheckBox = (By.XPATH, "//input[@data-name='preferences-item-cb' and @type='checkbox']")
    markedChecklistsCheckBox_CL_Names = (By.XPATH , "//input[@type = 'checkbox' and @aria-checked='true']/../following-sibling::label")
    preferences_window = (By.XPATH, "//span[@class = 'cl-modal-title-text']")
    preferences_window_first_time_user = (By.XPATH, "//div[@class = 'cl-title']")
    doneButton = (By.XPATH, "//button//div[contains(text(),'done')]/.." )
    first_time_user_icon = (By.XPATH,"//div[@class='cl-preferences-popover-text']")
    preferences_window = (By.XPATH,"//span[contains(text(),'Preferences')]")
    owned_by_me_list = (By.XPATH,"//div[@data-name='drawer-owned-ip-row']")

    assign_version_manually_button = (By.XPATH, "//*[@class='v-btn__content' and contains(text(),'Assign Version Manually')]")
    version_radio_button = "//*[@class='cl-align-center' and contains(text(),\'"
    toolbar_development_toggle_btn=(By.XPATH,"//button[@data-id='toolbar-development-toggle-btn']")
    toolbar_released_toggle_btn=(By.XPATH,"//button[@data-id='toolbar-released-toggle-btn']")





