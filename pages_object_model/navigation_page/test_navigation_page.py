import os
import allure_pytest
import allure
from utils.allure_utils import attach_png
import time
import pytest
import unittest
from selenium.webdriver.support.ui import Select
from selenium import webdriver
from selenium.webdriver.common.by import By
from pages_object_model.navigation_page.navigation_page import NavigationPage

class NavigationPageTest(unittest.TestCase):

    @classmethod
    # @pytest.fixture()
    def setUpClass(cls):
        # Open the Home page URL
        cls.driver = webdriver.Chrome(executable_path=os.path.join(os.getcwd(), "bin", "chromedriver.exe"))
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()
        cls.driver.get("https://pearl-quality-qa.app.intel.com/")

    @allure.feature('My Feature Of NavigationPageTest')
    @allure.story('My Story test search by group')
    def test_search_by_group(self):
            navigation_page = NavigationPage(self.driver)
            navigation_page.click_on_select_ip_configuration_button()
            attach_png(self.driver.get_screenshot_as_png(), "ip_configuration Image")
            navigation_page.click_on_select_defferent_ip_button()
            attach_png(self.driver.get_screenshot_as_png(), "defferent_ip Image")
            navigation_page.select_your_ip_by_group()
            attach_png(self.driver.get_screenshot_as_png(), "by_group Image")
            navigation_page.click_on_select_pin_button()
            navigation_page.click_on_HOME__button()
            navigation_page.click_on_select_ip_configuration_button()
            navigation_page.validate_ip_configuration_in_favorite_list(navigation_page.Configuration)



    # def test_Settings_Checked_AllCL_Type(self):
    #     try:
    #         navigation_page = NavigationPage(self.driver)
    #         time.sleep(3)
    #         navigation_page.ClickOnSettings()
    #         navigation_page.AllCheckboxesAreFull()
    #     finally:
    #         print ( "-" * 80 + "End of test Settings_AllCL" + "-" * 80 )
    #
    # def test_Settings_Save_Disable(self):
    #     try:
    #         navigation_page = NavigationPage(self.driver)
    #         navigation_page.ClickOnSettings()
    #         navigation_page.EmptyAllCheckBoxesAndClickOnSave()
    #     finally:
    #         print ( "-" * 80 + "End of test Settings_ZeroCL" + "-" * 80 )
    #
    # def test_Settings_SomeCL(self):
    #     try:
    #         navigation_page = NavigationPage(self.driver)
    #         navigation_page.ClickOnSettings()
    #         navigation_page.ChooseRandomCL()
    #     finally:
    #         print ( "-" * 80 + "End of test Settings_ZeroCL" + "-" * 80 )

    @classmethod
    def tearDownClass(cls):
        # cls.driver.quit()
        cls.driver.close()
        print("Test completed")


if __name__ == "__main__":
    # unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output="C:/Users/ezarourx/PycharmProjects/apt2-automation/Allure"))
    unittest.main()
