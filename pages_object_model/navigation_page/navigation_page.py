# from element import BasePageElement
# from navigation_page_locators import NavigationPageLocators
import random
import time
import unittest
from telnetlib import EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from entities.drops_entity.drop_checklist_entity import DropChecklists
from pages_object_model.base_page.base_page_web import BasePageWeb
from pages_object_model.navigation_page.navigation_page_locators import NavigationPageLocators
from utils import connectToDB

class NavigationPage(BasePageWeb):
    def __init__(self, driver):
        self.Configuration = ""
        self.db_milestone_result={}
        #In the UI
        self.Search_milestone_ID_milstoneID = ""
        self.Search_milestone_ID_revision = ""
        self.Search_milestone_ID_list_of_checklistName ={}
        self.Search_milestone_ID_list_of_version = {}
        self.Search_milestone_ID_list_of_locked = {}
        self.driver=driver

    def set_Configuration(self,configName):
        self.Configuration = configName

    def get_Configuration(self) :
        return self.Configuration

    def is_on_home_page(self):
        try:
            self.driver.find_element(*NavigationPageLocators.selectIPConfiguration_Button)
        except NoSuchElementException:
            return False
        return True

    def click_on_select_ip_configuration_button(self):
        if(self.wait_to_be_displayed(*NavigationPageLocators.selectIPConfiguration_Button)):
            self.wait_and_click_on_element(*NavigationPageLocators.selectIPConfiguration_Button)

    def click_on_select_different_ip_button(self):
        self.wait_and_click_on_element(*NavigationPageLocators.selectDefferentIP_Button)

    def select_your_ip_by_group(self,Group,Supplier,Generation,Configuration):
        self.Configuration = Configuration
        if Supplier is None:
            if(self.wait_to_be_displayed(By.XPATH,NavigationPageLocators.allConfiguration_arrow_drop_down)):
                self.wait_and_click_on_element(By.XPATH, NavigationPageLocators.allConfiguration_arrow_drop_down )
                self.driver.find_element_by_xpath ( NavigationPageLocators.allConfiguration_arrow_drop_down ).send_keys(self.Configuration)
            if (self.wait_to_be_displayed (By.XPATH, NavigationPageLocators.allConfiguration_option)):
                self.driver.find_element_by_xpath(NavigationPageLocators.allConfiguration_option).click()
        else:
            if (self.wait_to_be_displayed (By.XPATH,NavigationPageLocators.allGroup_arrow_drop_down)):
                self.wait_and_click_on_element(By.XPATH,NavigationPageLocators.allGroup_arrow_drop_down)
                self.driver.find_element_by_xpath(
                    '{0}{1}{2}'.format(NavigationPageLocators.dropdown_option, Group, "\')]")).click()
                self.driver.find_element_by_xpath(NavigationPageLocators.allSuppliers_arrow_drop_down).click()
                self.driver.find_element_by_xpath(
                    '{0}{1}{2}'.format(NavigationPageLocators.dropdown_option, Supplier, "\')]")).click()
                self.driver.find_element_by_xpath(NavigationPageLocators.allGenerations_arrow_drop_down).click()
                self.driver.find_element_by_xpath(
                    '{0}{1}{2}'.format(NavigationPageLocators.dropdown_option, Generation, "\')]")).click()
                self.driver.find_element_by_xpath(NavigationPageLocators.allConfiguration_arrow_drop_down).click()
                self.driver.find_element_by_xpath(
                    '{0}{1}{2}'.format(NavigationPageLocators.dropdown_option, Configuration, "\')]")).click()

    def click_on_select_button(self):
        self.wait_and_click_on_element(*NavigationPageLocators.select_Button)

    def click_on_select_pin_button(self):
        self.wait_and_click_on_element (*NavigationPageLocators.select_and_pin_Button)
        # self.driver.find_element(*NavigationPageLocators.select_and_pin_Button).click()

    def validate_ip_configuration_in_favorite_list(self, expected_title):
        actual_ip_configuration=self.driver.find_elements_by_class_name(NavigationPageLocators.favorites_list)
        actual_ipConfig_name= ""
        for elem in actual_ip_configuration:
            if expected_title in elem.text:
                actual_ipConfig_name=elem.text
                break
        assert expected_title in actual_ipConfig_name,"IP Configuration : %s does not found " %(expected_title)

    def get_3_Recommended_ip_configuration_checklist(self):
            return self.driver.find_elements(*NavigationPageLocators.Recommended_Checklist_IP_Names)

    def update_Drops_Recommended(self):
        time.sleep(0.5)
        drops_Recommended_locator=self.driver.find_elements(*NavigationPageLocators.Recommended_Checklist_IP_Names)
        self.dropChecklistsList = []
        for drop in drops_Recommended_locator:
            self.dropChecklistsList.append(DropChecklists(drppt_type=drop.text))

    def click_on_cancel_button(self):
        self.driver.find_element_by_xpath(NavigationPageLocators.cancel_Button).click()

    def search_milestone_ID_textfield(self,milestone_ID,checklistName=None):
        self.driver.find_element(*NavigationPageLocators.Search_milestone_ID).send_keys(milestone_ID)
        if(self.wait_to_be_displayed(*NavigationPageLocators.Search_milestone_ID_list_of_checklistName)):
            self.Search_milestone_ID_list_of_checklistName = list(map(lambda x:x.text,self.driver.find_elements(*NavigationPageLocators.Search_milestone_ID_list_of_checklistName)))
            self.Search_milestone_ID_list_of_version = list(map(lambda x:x.text,self.driver.find_elements(*NavigationPageLocators.Search_milestone_ID_list_of_version)))
            self.Search_milestone_ID_list_of_locked = list(map(lambda x:x.text,self.driver.find_elements(*NavigationPageLocators.Search_milestone_ID_list_of_locked)))
            self.Search_milestone_ID_revision = self.driver.find_element(*NavigationPageLocators.Search_milestone_ID_revision).text

    def search_milestone_ID_get_error(self):
        return self.driver.find_element(*NavigationPageLocators.Search_milestone_ID_error).text

    def choose_checklist_name_by_search_milestone_ID(self,checklistName):
        if(self.wait_to_be_displayed(*NavigationPageLocators.Search_milestone_ID_list_of_checklistName)):
            self.driver.find_element_by_xpath('{0}{1}{2}'.format(NavigationPageLocators.Search_milestone_ID_checklistName,checklistName,"\')]")).click()

    def get_milestone_ID_SQL_by_mode(self,env,locked,mode='released'):
        self.db = connectToDB.SQLQuiries(env)
        # self.db_milestone_result = connectToDB.SQLQuiries.get_milestone_id_by_locked(self.db, locked)
        self.db_milestone_result = connectToDB.SQLQuiries.get_milestone_id_by_locked_and_mode(self.db, locked, mode)
        return self.db_milestone_result

    def get_milestone_ID_SQL_by_checklistName(self,env,checklistName):
        self.db = connectToDB.SQLQuiries(env)
        self.db_milestone_result = connectToDB.SQLQuiries.get_checklist_id_by_checklistName_and_version(self.db, checklistName)
        return self.db_milestone_result

