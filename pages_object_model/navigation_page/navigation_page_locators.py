from selenium.webdriver.common.by import By


class NavigationPageLocators(object):

    Home_button=(By.XPATH,"//button//div[contains(text(),'Home')]")
    selectIPConfiguration_Button = (By.XPATH, "//div[@data-id='home-select-ip-btn']")

    cancel_Button = (By.XPATH, "//*[@data-id='di-pop-select-ip-cancel-btn']")
    select_Button = (By.XPATH, "//*[@data-id='di-pop-select-ip-btn']")

    select_and_pin_Button = (By.XPATH, "//div[@data-id='di-pop-select-and-pin-ip-btn']")
    selectDefferentIP_Button = (By.XPATH, "//*[@data-id='drawer-select-different-ip-btn']")

    allGroup_arrow_drop_down = "//input[@aria-label='All Groups']"
    allSuppliers_arrow_drop_down = "//input[@aria-label='All Suppliers']"
    allGenerations_arrow_drop_down = "//input[@aria-label='All Generations']"
    allConfiguration_arrow_drop_down = "//input[@aria-label='All IP Configuration']"
    allConfiguration_option = "//*[@class = 'v-list__tile__mask']"
    dropdown_option= "//div[contains(text(),\'"

    settingsButton = (By.XPATH,"//button[@data-id='toolbar-settings-btn']")
    unMarkedChecklistsCheckBox = (By.XPATH,"//input[@type = 'checkbox' and @aria-checked='false']")
    markedChecklistsCheckBox =  (By.XPATH,"//input[@type = 'checkbox' and @aria-checked='true']")
    markedChecklistsCheckBox_CL_Names =  (By.XPATH,"//input[@type = 'checkbox' and @aria-checked='true']/../following-sibling::label")
    saveButton = (By.XPATH, "//div[@data-id='modal-Save-btn']")
    preferences_window = (By.XPATH, "//span[@class = 'cl-modal-title-text']")
    preferences_window_first_time_user = (By.XPATH, "//div[@class = 'cl-title']")
    doneButton = (By.XPATH, "//button//div[contains(text(),'done')]/.." )

    favorites_list= "cl-list-item" # by class name

    Recommended_Checklist_IP_Names=(By.XPATH, "//div[@class='cl-ip-name']")

    Search_milestone_ID=(By.XPATH,"//*[@placeholder='Search milestone ID']")
    Search_milestone_ID_revision=(By.XPATH, "//*[@class='cl-milestone-results-header']")
    Search_milestone_ID_list_of_checklistName = (By.XPATH, "//*[@class='flex text-xs-left xs4']")
    Search_milestone_ID_checklistName = "//*[@class='flex text-xs-left xs4' and contains(text(),\'"
    Search_milestone_ID_list_of_version = (By.XPATH, "//*[@class='flex text-xs-center cl-checklist-version-label xs4']")
    Search_milestone_ID_list_of_locked = (By.XPATH, "//*[@class='v-icon cl-locked-checklist-icon material-icons theme--light']")
    Search_milestone_ID_error = (By.XPATH, "//*[@class='cl-milestone-search-error']")





