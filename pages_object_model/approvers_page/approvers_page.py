import time
import random
import unittest
from telnetlib import EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from entities.drops_entity.drop_checklist_entity import DropChecklists
from pages_object_model.base_page.base_page_web import BasePageWeb
from pages_object_model.navigation_page.navigation_page import NavigationPage
from pages_object_model.drops_page.drops_page import DropsPage
from pages_object_model.navigation_page.navigation_page_locators import NavigationPageLocators
from pages_object_model.common_page.common_page import CommonPage
from pages_object_model.drops_page.drops_page_locators import DropsPageLocators
from pages_object_model.approvers_page.approvers_page_locators import ApproversPageLocators
from pages_object_model.checklist_page.checklist_page_locators import ChecklistPageLocators
from utils import connectToDB
from selenium.webdriver import ActionChains
from datetime import datetime
from selenium.webdriver.common.action_chains import ActionChains

import re



class ApproversPage(BasePageWeb):

    def click_on_thumbs_up(self):
        self.click_on_scrollbar_until_element_seen(ApproversPageLocators.Scroll_bar,ApproversPageLocators.Thumb_up_icon)
        time.sleep(1)
        self.wait_and_click_on_element(*ApproversPageLocators.Thumb_up_icon)
        time.sleep(1)

    def click_on_thumbs_down(self):
        self.click_on_scrollbar_until_element_seen(ApproversPageLocators.Scroll_bar,ApproversPageLocators.Thumb_down_icon)
        if (self.wait_to_be_displayed ( *ApproversPageLocators.Thumb_down_icon)):
            elem = self.driver.find_element(*ApproversPageLocators.Thumb_down_icon)
            self.driver.execute_script("arguments[0].click();",elem)

    def click_on_submit(self):
        if(self.wait_to_be_displayed(*ApproversPageLocators.Submit)):
            self.wait_and_click_on_element(*ApproversPageLocators.Submit)

    def make_approver(self,env,ipconfigName,username):
        self.db = connectToDB.SQLQuiries(env)
        connectToDB.SQLQuiries.make_approver_by_ipConfigName(self.db, ipconfigName, username)

    def delete_approver(self,env,username,ipconfigName):
        self.db = connectToDB.SQLQuiries(env)
        connectToDB.SQLQuiries.delete_approver_by_ipConfigName_and_idsid(self.db, username, ipconfigName)

    def get_approver(self,env,username,ipconfigName):
        self.db = connectToDB.SQLQuiries(env)
        approver = list( filter( lambda x: x[0]==username , connectToDB.SQLQuiries.get_approver_by_ipconfigName(self.db, ipconfigName)))
        return approver

    def sets_as_approver(self,env,username,ipconfigName):
        self.db = connectToDB.SQLQuiries(env)
        connectToDB.SQLQuiries.delete_approver_by_ipConfigName_and_idsid(self.db, username, ipconfigName)
        connectToDB.SQLQuiries.sets_as_approver_by_ipconfigName(self.db, username, ipconfigName)

    def get_all_requests_list(self):
        if(self.wait_to_be_displayed(*ApproversPageLocators.Submit)):
            return self.driver.find_elements(*ApproversPageLocators.Requester_justification_list)