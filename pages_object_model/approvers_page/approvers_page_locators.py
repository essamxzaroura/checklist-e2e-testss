from selenium.webdriver.common.by import By
from datetime import datetime

class ApproversPageLocators(object):

    justification_text = 'JUSTIFICATION_PEARL_QA_AUTOTEST'
    Requester_justification_list = (By.XPATH, ("//div[@col-id='requesterJustification']//div[contains(text()," + justification_text + ")]/parent::*/following-sibling::div[@col-id='response']"))
    Submit = (By.XPATH, "//*[@data-id = 'approvers-submit-btn']")
    Thumb_up_icon=(By.XPATH, "//div[@col-id='requesterJustification']//*[contains(text()," + justification_text + ")]/parent::*/parent::*/*[@col-id ='response']//*[contains(text(), 'thumb_up')]")
    Thumb_down_icon=(By.XPATH,"//div[@col-id='requesterJustification']//*[contains(text()," + justification_text + ")]/parent::*/parent::*/*[@col-id ='response']//*[contains(text(), 'thumb_up')]")
    Scroll_bar = (By.XPATH,"//*[@class='ag-body-horizontal-scroll-viewport']")
    Requester_justification_list=(By.XPATH,"//div[@col-id='requesterJustification']")