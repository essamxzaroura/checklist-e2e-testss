from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
import time

# this Base class is serving basic attributes for every single page inherited from Page class

class BasePageWeb(object):
    def __init__(self, driver, base_url='https://pearl-quality-qa.app.intel.com/'):
        self.base_url = base_url
        self.driver = driver
        self.timeout = 30

    def find_element(self, *locator):
        return self.driver.find_element(*locator)

    def open(self,url):
        url = self.base_url + url
        self.driver.get(url)

    def get_title(self):
        return self.driver.title

    def get_url(self):
        return self.driver.current_url

    def hover(self, *locator):
        element = self.find_element(*locator)
        hover = ActionChains(self.driver).move_to_element(element)
        hover.perform()

    def refresh(self):
        self.driver.refresh()
        time.sleep(2)
        # add pearl-cookie

    def go_back(self):
        self.driver.back()


    def wait_and_click_on_element(self,*locator):
        try:
           element = WebDriverWait(self.driver, 3).until(EC.element_to_be_clickable((locator)))
           element.click()
        finally:
            return True

    def wait_to_be_displayed(self,*locator):
        try:
            element = WebDriverWait(self.driver, 3).until(EC.presence_of_all_elements_located(locator))
            return True
        except:
            return False

    def wait_for_element_be_invisible(self, *locator):
        try:
            element = WebDriverWait(self.driver, 3).until(EC.invisibility_of_element_located(locator))
            return True
        except:
            return False

    def click_on_scrollbar_until_element_seen(self,scrollBar_locator,element_locator):
        if (len(self.driver.find_elements(*element_locator))<1):
            self.driver.find_element(*scrollBar_locator).click()










