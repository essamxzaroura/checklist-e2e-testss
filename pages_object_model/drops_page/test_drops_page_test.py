import os
import allure_pytest
import allure

from pages_object_model.drops_page.drops_page import DropsPage
from utils.allure_utils import attach_png
import pytest
import unittest
from selenium import webdriver
from pages_object_model.navigation_page.navigation_page import NavigationPage

class NavigationPageTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Open the Home page URL
        cls.driver = webdriver.Chrome(executable_path=os.path.join(os.getcwd(), "bin", "chromedriver.exe"))
        cls.driver.implicitly_wait(10)
        # cls.driver.maximize_window()
        cls.driver.get("https://pearl-quality-qa.app.intel.com/")

    @allure.feature('My Feature Of NavigationPageTest')
    @allure.story('My Story test search by group')
    def test_Presented_only_4_Milestones_and_be_clickable(self):
            navigation_page = NavigationPage(self.driver)
            navigation_page.click_on_select_ip_configuration_button()
            attach_png(self.driver.get_screenshot_as_png(), "ip_configuration Image")
            navigation_page.click_on_select_defferent_ip_button()
            attach_png(self.driver.get_screenshot_as_png(), "defferent_ip Image")
            navigation_page.select_your_ip_by_group("Atom Cores","Atom Cores","Airmont Gen_1.0","ATOM-AIRMONT_1P0-1222")
            attach_png(self.driver.get_screenshot_as_png(), "by_group Image")
            navigation_page.click_on_select_button()
            drops_page = DropsPage(self.driver)
            drops_page.validate_title_of_drops_page(navigation_page.Configuration)
            drops_page.validate_only_4_milestones_drops_page(drops_page.get_4_milestones_drops_page())

    @classmethod
    def tearDownClass(cls):
        # cls.driver.quit()
        cls.driver.close()
        print("Test completed")

if __name__ == "__main__":
    unittest.main()
