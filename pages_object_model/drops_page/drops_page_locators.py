from selenium.webdriver.common.by import By

class DropsPageLocators(object):
    IPConfiguration_title_text_old = "cl-title"  # by class name
    IPConfiguration_title_text="//div[@class='cl-title']"
    Milestones_arrow ="//div[@class='cl-milestone-title']"
    Filter_Button = (By.XPATH, "//*[@data-id='drops-filter-btn']")
    Filter_Check_Box = (By.CLASS_NAME, "v-input--selection-controls__ripple")
    Filter_Select_Button = (By.XPATH, "//*[@data-id='drops-apply-filter-btn']")
    Drop_Checklist = (By.XPATH, "//div[@class='cl-drop']")
    ChecklistsList_tile = (By.XPATH, "//div[@data-name='drops-drop-checklist-row']")
    Drop_Tile_Checklist = (By.XPATH, "//div[@data-drop-tile-checklist='RTL']")
    Drop_Checklist_Row = (By.XPATH, "//div[@data-name='drops-drop-checklist-row']")
    Select_Range_Button= (By.XPATH,"//div[@data-id='range-select-btn']")
    Select_Milestone_Button=(By.XPATH,"//div[@data-id='drops-milestones-btn']")
    Mileston_Scroll=(By.XPATH,"//div[@class='v-slider__ticks-container']/span")
    Cancel_Select_Milestone=(By.XPATH,"//div[@data-id='range-cancel-btn']")
    Default_Selection_Button=(By.XPATH,"//div[@data-id='range-default-btn']")
    Drop_selected_In_Mileston_Scroll=(By.XPATH,"//div[@class='v-slider']/input")

    def get_Drop_Checklists_Rows(self, milestoneID):
        Drop_Checklist_Rows = (By.XPATH, ("//div[@data-drops-milestone='"+milestoneID+ "']/ancestor::*/following::div[@class='cl-summary-rows-wrapper']/child::*"))
        return Drop_Checklist_Rows
