
from telnetlib import EC
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from entities.drops_entity.drop_checklist_entity import DropChecklists
from entities.drops_entity.drop_checklist_entity import Checklist_Row
from pages_object_model.base_page.base_page_web import BasePageWeb
from pages_object_model.drops_page.drops_page_locators import DropsPageLocators
from pages_object_model.navigation_page.navigation_page import NavigationPage
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver import ActionChains
from utils.allure_utils import attach_png


class DropsPage(BasePageWeb):
    def __init__(self, driver):
        self.driver=driver
        self.drops_IP_Configuration_Checklist = []
        self.checklist_Row=[]
        self.action_chains = ActionChains(driver)

    def click_on_Filter_button(self):
        self.wait_and_click_on_element(*DropsPageLocators.Filter_Button)

    def checked_lock_Filter(self):
        self.wait_and_click_on_element(*DropsPageLocators.Filter_Check_Box)

    def click_on_Filter_Select_Button(self):
        self.wait_and_click_on_element(*DropsPageLocators.Filter_Select_Button)

    def click_on_drop_tile_checklist(self,title_checklist):
        self.wait_and_click_on_element(*DropsPageLocators.Drop_Tile_Checklist)

    def update_drop_ip_configuration_checklist(self):
        drops_ip_configuration_checklist_locator=self.driver.find_elements(*DropsPageLocators.Drop_Checklist)
        self.dropChecklistsList = []
        for drop in drops_ip_configuration_checklist_locator:
            result=drop.text.split('\n')
            self.dropChecklistsList.append(DropChecklists(drppt_type=result[0], tren_ww=result[1], revision=result[2], milestone_id=result[3], drop_tile_checklist=result[4], versionName=result[5]))

    def update_checklists_in_drops_page(self):
        for row in self.driver.find_elements(*DropsPageLocators.Drop_Checklist_Row):
            list=row.text.split('\n')
            self.checklist_Row.append(Checklist_Row(list))

    def validate_title_of_drops_page(self,expected_title):
        actual_title=self.driver.find_element_by_xpath(DropsPageLocators.IPConfiguration_title_text).text
        assert expected_title in actual_title, "IP Configuration Title Wrong: %s " % (expected_title)

    def validate_only_4_milestones_drops_page(self,actual_4_milestones_drops_page):
        assert 4 == len(actual_4_milestones_drops_page), "we have %s milestone " % len(actual_4_milestones_drops_page)


    def get_4_milestones_drops_page(self):
        return self.driver.find_elements_by_xpath(DropsPageLocators.Milestones_arrow)

    def clickOnRelevantChecklist(self, checklistName, milestoneID):
        if(self.wait_to_be_displayed(*DropsPageLocators.get_Drop_Checklists_Rows(self ,milestoneID))):
            ChecklistsOnDropPage = self.driver.find_elements(*DropsPageLocators.get_Drop_Checklists_Rows(self,milestoneID))
            for elem in ChecklistsOnDropPage:
                if checklistName.lower() in elem.text.lower():
                    elem.click()
                    break

    def click_on_Milestones_button(self):
        self.wait_and_click_on_element(*DropsPageLocators.Select_Milestone_Button)

    def click_on_Cancel_Select_Milestone_button(self):
        self.wait_and_click_on_element(*DropsPageLocators.Cancel_Select_Milestone)

    def click_on_Default_Selection_button(self):
        self.wait_and_click_on_element(*DropsPageLocators.Default_Selection_Button)

    def click_on_Select_Range_button(self):
        self.wait_and_click_on_element(*DropsPageLocators.Select_Range_Button)

    def select_Milestone_Range_Drop(self,dropName):
        self.click_on_Milestones_button()
        Mileston_Scroll_Drops =self.driver.find_elements(*DropsPageLocators.Mileston_Scroll)
        rangeֹ_appears_on_Scroll=self.driver.find_elements(*DropsPageLocators.Drop_selected_In_Mileston_Scroll)
        range_value= int (rangeֹ_appears_on_Scroll[1].get_attribute("value"))
        source=Mileston_Scroll_Drops[range_value]
        for elem in Mileston_Scroll_Drops:
            if elem.text==dropName:
                target=elem
        self.action_chains.drag_and_drop(source, target).perform()
        time.sleep(1)
        self.click_on_Select_Range_button()

    def is_dropname_in_droppage(self,dropName):
        time.sleep(1)
        flag = False
        milestones_drops=self.get_4_milestones_drops_page()
        for elem in milestones_drops:
            if elem.text == dropName:
                flag=True
        return flag