from behave import then
from hamcrest import *
from pages_object_model.common_page.common_page_locators import CommonPageLocators
from utils.allure_utils import attach_png

@then("validate 4 Recommended ip configuration checklist")
def step_impl(context):
    actual_Recommended_ip_configuration_checklist = context.navigation_page.get_3_Recommended_ip_configuration_checklist()
    assert_that(len(actual_Recommended_ip_configuration_checklist),equal_to(4),attach_png((context.web._web_driver).get_screenshot_as_png(),'validate'))

@then("validate Order of 4 Recommended 1.{first} 2.{second} 3.{third} 4.{fourth}")
def step_impl(context, first, second, third,fourth):
    assert_that(context.navigation_page.dropChecklistsList[0].drppt_type, contains_string(first))
    assert_that(context.navigation_page.dropChecklistsList[1].drppt_type, contains_string(second))
    assert_that(context.navigation_page.dropChecklistsList[2].drppt_type, contains_string(third))
    assert_that(context.navigation_page.dropChecklistsList[3].drppt_type, contains_string(fourth))

@then("validate in drops page checklistname BY-DB is versionName: {versionName}")
@then("validate in drops page checklistname:{checklistName} is versionName: {versionName}")
def step_impl(context, versionName, checklistName=None):
    if not (checklistName):
        checklistName = context.db_result.db_checklistName
    for row in context.drops_page.checklist_Row:
        if row.ui_checklistName == checklistName:
            assert_that(row.ui_versionName, equal_to(versionName), 'checklistName is not  %s version' % versionName)
            break

@then("validate only 4 milestones drops page")
def step_impl(context):
    actual_4_milestones_drops_page= context.drops_page.get_4_milestones_drops_page()
    assert_that(len(actual_4_milestones_drops_page), equal_to(4),'field %s value was wrong' % len(actual_4_milestones_drops_page))

@then("validate all the checklists are checked")
def step_impl(context):
    if (context.base_page.wait_to_be_displayed(*CommonPageLocators.settingsButton)):
        assert_that(len(context.actual_unchecked_checklists),equal_to(0),'Preferences choice was not saved correctly')

@then('validate checklist is not present type')
@then('validate checklist is not present type {type}')
def step_impl(context,type='HAS'):
    for drop in context.drops_page.dropChecklistsList:
        assert_that((drop.drop_tile_checklist),not equal_to(type),'checklist %s type' % type)

@then('validate checklist is present type')
@then('validate checklist is present type {type}')
def step_impl(context,type='HAS'):
    for drop in context.drops_page.dropChecklistsList:
        assert_that((drop.drop_tile_checklist),equal_to(type),'checklist %s type' % type)

@then("validate the checklists preferences window opens")
def step_impl(context):
    actual_preferences_window = context.preferences_window_first_time_user
    assert_that(actual_preferences_window, equal_to("Which Checklists are you interested in?"))

@then("validate the done button is disabled")
def step_impl(context):
    acrual_done_button_click_status = context.common_page.get_done_button().is_enabled()
    assert_that(acrual_done_button_click_status, equal_to(False))
    context.web._web_driver.refresh()

@then("validate setted preferences checklists are checked")
def step_impl(context):
    assert_that(context.common_page.get_checked_checklists_names(), equal_to(context.common_page.Checked_after_random_lst))

@then("validate the grade was changed")
def step_impl(context):
    actual_grade = context.checklist_page.get_grade()
    assert_that(actual_grade, not(equal_to(context.checklist_page.grade)))

@then("validate copied rules results")
def step_impl(context):
    assert_that(context.checklist_page.changedRules2_result, equal_to(context.checklist_page.changedRules2_result))

@then("validate the rules are presented")
def step_impl(context):
    actual_rules_names = context.checklist_page.get_rules_names()
    actual_result = True
    for elem in actual_rules_names:
        if not elem.text:
            actual_result = False
            break
    assert_that(actual_result, equal_to(True))

@then("validate copied rules with approval requests only")
def step_impl(context):
        expected_request = list(context.checklist_page.changedRules1.items())[0]
        expected_non_request = list(context.checklist_page.changedRules1.items())[1]
        actual_request = list(context.checklist_page.changedRules2.items())[0]
        actual_non_request = list(context.checklist_page.changedRules2.items())[1]
        assert_that(expected_non_request,is_not(equal_to(actual_non_request)))
        assert_that(expected_request,equal_to(actual_request))

@then("validate IPs Owned By Me")
def step_impl(context):
    actual_owned_by_me_list = context.common_page.get_owned_by_me_list()
    actual_result = False
    for elem in actual_owned_by_me_list:
        if elem.text==context.db_result.db_ipConfigName:
            actual_result = True
            break
    assert_that(actual_result,equal_to(True), 'IP is not found in the Owned By Me List')

@then("validate search milestone shows the checklistName")
def step_impl(context):
    actual = list(filter(lambda x: x == context.navigation_page.sql_milestone_result[0][1],context.navigation_page.Search_milestone_ID_list_of_checklistName))
    assert_that((actual[0]),equal_to(context.navigation_page.sql_milestone_result[0][1]),'checklist %s type' %(context.navigation_page.sql_milestone_result[0][1]))

@then("validate search milestone shows the checklistName and version")
def step_impl(context):
    i = 0
    while i < len(context.navigation_page.Search_milestone_ID_list_of_checklistName):
        if (context.db_result.db_checklistName)==(context.navigation_page.Search_milestone_ID_list_of_checklistName[i]):
            versionName=context.db_result.db_versionName
            assert_that((versionName if (versionName!=None) else "Get Started"),equal_to(context.navigation_page.Search_milestone_ID_list_of_version[i]))
            # assert_that(context.db_result.db_isLocked,equal_to(context.navigation_page.Search_milestone_ID_list_of_locked[i]))
            break
        i = i + 1

@then("validate search milestone shows the revision")
def step_impl(context):
    assert_that(context.navigation_page.Search_milestone_ID_revision, equal_to(context.db_result.db_revision))

@then("validate search milestone throw error not found")
def step_impl(context):
    assert_that(context.navigation_page.search_milestone_ID_get_error(), contains_string("Milestone ID not found"),'Milestone ID is found ')

@then("validate checklist page shows the details")
def step_impl(context):
    context.checklist_page.update_page_details()
    assert_that(str(context.checklist_page.checklistName_label), contains_string(context.db_result.db_checklistName),'checklistName expected %s '%(context.db_result.db_checklistName))
    assert_that(str(context.checklist_page.checklist_versionName_label), equal_to(context.db_result.db_versionName),'versionName expected %s '%(context.db_result.db_versionName))
    assert_that(str(context.checklist_page.ipConfigurationName_label), equal_to(context.db_result.db_ipConfigName),'ipConfigurationName expected %s '%(context.db_result.db_ipConfigName))
    assert_that(int(context.checklist_page.checklist_milestoneid_label), equal_to(context.db_result.db_milestoneID),'milestoneid expected %s '%(context.db_result.db_milestoneID))

@then('validate Navigation page is not presented checklistName {checklistName} in search milestone')
def step_impl(context,checklistName):
    for checklistName in context.navigation_page.Search_milestone_ID_list_of_checklistName:
        assert_that(checklistName,not equal_to(checklistName),'checklist name %s Found' % checklistName)

@then('verify approved request "{index}"')
def step_impl(context,index):
    actual_request_status = context.checklist_page.get_approval_status(int(index))
    assert_that(actual_request_status,equal_to("Approved"))

@then('verify in process request "{index}"')
def step_impl(context,index):
    actual_request_status = context.checklist_page.get_approval_status(int(index))
    assert_that(actual_request_status,equal_to("In Process"))

@then('verify rejected request "{index}"')
def step_impl(context,index):
    actual_request_status = context.checklist_page.get_approval_status(int(index))
    assert_that(actual_request_status,equal_to("Rejected"))

@then('validate the rule {index} is "{response}"')
def step_impl(context,index,response):
    actual_response=context.checklist_page.get_response_rule_by_index(int(index))
    if actual_response == '':
        actual_response = 'No Response'
    assert_that(actual_response,equal_to(response))

@then('validate approval request is not found')
def step_impl(context):
    list=context.approvers_page.get_all_requests_list()
    for request in list:
        if(request.text!='Requester Justification'):
            assert_that((request.text),not contains('JUSTIFICATION_PEARL_QA_AUTOTEST'),'request found in this mode')
