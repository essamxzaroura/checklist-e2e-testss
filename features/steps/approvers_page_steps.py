from behave import when,given
import time




from behave import given , when , then
from pages_object_model.base_page.base_page_web import BasePageWeb
from pages_object_model.checklist_page.checklist_page import ChecklistPage
from pages_object_model.navigation_page.navigation_page_locators import NavigationPageLocators
from pages_object_model.drops_page.drops_page_locators import DropsPageLocators
from pages_object_model.checklist_page.checklist_page_locators import ChecklistPageLocators
from pages_object_model.common_page.common_page import CommonPage
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

@given('Get the latest checklist information approvers')
def step_impl(context):
    context.db_result.update_db(context.checklist_page.select_all_checklists_from_DB_app(context.config.userdata['environment']))

@when('user sets approver')
@when('user sets approver "{checklistNumber}"')
def step_impl(context, checklistNumber='1'):
    if(checklistNumber=='2'):
        ipConfig = context.DB_ipConfigurationName2Con
    else:
        ipConfig = context.db_result.db_ipConfigName
    approver = context.approvers_page.get_approver(context.config.userdata['environment'],context.config.userdata['user'],ipConfig)
    if (len(approver) == 0):
        context.approvers_page.make_approver(context.config.userdata['environment'],ipConfig,context.config.userdata['user'])

@when('user sets as approver')
@when('user sets as approver "{checklistNumber}"')
def step_impl(context, checklistNumber='1'):
    if(checklistNumber=='2'):
        ipConfig = context.db_result_2_checklists.db_ipConfigName2
    else:
        ipConfig = context.db_result.db_ipConfigName
    context.approvers_page.sets_as_approver(context.config.userdata['environment'],context.config.userdata['user'],ipConfig)

@when('user approves request')
def step_impl(context):
    context.approvers_page.click_on_thumbs_up()
    context.approvers_page.click_on_submit()

@when('user rejects request')
def step_impl(context):
    context.approvers_page.click_on_thumbs_down()
    context.approvers_page.click_on_submit()

@when('delete the user from approvers')
def step_impl(context):
    context.approvers_page.delete_approver(context.config.userdata['environment'],context.config.userdata['user'],context.db_result.db_ipConfigName)
