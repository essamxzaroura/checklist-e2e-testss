from behave import when,given
import time

@when('user go to HOME page')
def step_impl(context):
    time.sleep(1.5)
    context.common_page.click_on_HOME__button()
    time.sleep(0.5)

@when('user go to Back')
def step_impl(context):
    time.sleep(0.5)
    context.web.go_back()
    time.sleep(0.5)

@when('user goes to approvers page')
def step_impl(context):
    context.common_page.click_on_approvers_button()

@when('prefernces {uncheck} checklist type {selectType}')
@when('prefernces {uncheck} checklist type')
def step_impl(context, uncheck, selectType='HAS'):
    if uncheck == 'uncheck':
        context.common_page.checked_checkbox_checklist_type(check='true', selectType=selectType)
    else:
        context.common_page.checked_checkbox_checklist_type(check='false', selectType=selectType)

@when( "prefernces click save button" )
def step_impl(context):
    context.common_page.click_on_save_button()

@given( "click on the prefernces icon" )
def step_impl(context):
    context.common_page.click_on_settings_button()

@when( "user sets all the preferences checklists to be checked" )
def step_impl(context):
    context.actual_unchecked_checklists = context.common_page.get_unchecked_checklists ()
    if (context.common_page.get_done_button ()is not False):
        context.common_page.click_on_done_button ()
    else:
        context.common_page.click_on_save_button ()

@when("user randomly sets the preferences checklists to be checked")
def step_impl(context):
    context.common_page.ChooseRandomCL()

@when("user refreshes page")
def step_impl(context):
    context.common_page.refresh()

@when('user makes himself an owner of IP configuration "{ipconfigID}"')
@when('user makes himself an owner of IP configuration')
def step_impl(context,ipconfigID=None):
    if ipconfigID is None:
        ipconfigID=context.db_result.db_ipConfigID
    owner_result = context.common_page.get_owner(ipconfigID,context.config.userdata['environment'])
    #1st case does have Owner
    if (owner_result != "No owner"):
        owner = list(filter(lambda x: 'owner' in x, owner_result))[0]
        context.DB_ownerCon = owner_result.get(owner)
        context.common_page.update_owner(context.config.userdata['environment'], ipconfigID,context.config.userdata['user'])
    else:
        context.DB_ownerCon = ''
        context.common_page.make_owner(context.config.userdata['environment'], ipconfigID,context.config.userdata['user'])
    time.sleep(1)
    context.common_page.refresh()

@when('user makes himself an owner of an IP configuration of checklist "{ipconfigName}"')
def step_impl(context,ipconfigName):
    if(ipconfigName=='1'):
        owner_result = context.common_page.get_owner(context.db_result.db_ipConfigID,context.config.userdata['environment'])
        if(owner_result != "No owner"):
            owner = list(filter(lambda x:'owner' in x,owner_result))[0]
            context.DB_ownerCon = owner_result.get(owner)
            context.common_page.update_owner(context.config.userdata['environment'],context.DB_ipConfigurationID1Con,context.config.userdata['user'])
        else:
            context.DB_ownerCon = ''
            context.common_page.make_owner(context.config.userdata['environment'],context.DB_ipConfigurationID1Con,context.config.userdata['user'])
        context.common_page.refresh()
        context.navigation_page.click_on_select_ip_configuration_button()

@when('delete owner "{username}"')
def step_impl(context,username):
    if (username == '1'):
        if(context.DB_ownerCon!=''):
            context.common_page.update_owner(context.config.userdata['environment'],context.db_result.db_ipConfigID,context.DB_ownerCon)#return previous owner
        else:
            context.common_page.delete_owner(context.config.userdata['environment'],context.config.userdata['user'],context.db_result.db_ipConfigID)#delete added owner

@given('db-get checklist by version: {versionName}')
@given('db-get checklist by version: {versionName} and checklistName "{checklistName}"')
def step_impl(context,versionName , checklistName=None):
        context.db_result.update_db(context.db_con.get_checklist_id_by_checklistName_and_version(versionName, checklistName))

@when('assign version manually to versionName: {versionName}')
def step_impl(context, versionName):
    if (context.common_page.select_version_if_get_started()):
        context.common_page.select_version(versionName)

@when('switch mode to: {mode}')
def step_impl(context, mode):
    context.common_page.switch_mode(mode)
