from behave import when,then
from pages_object_model.drops_page.drops_page import DropsPage

@when('user go to Home page {env} envirnment')
def step_impl(context, end):
    # context.current_page.select_store_from_list(end)
    print("go to home page")

#Drops steps
@when('click on filter button')
def step_impl(context):
    context.drops_page.click_on_Filter_button()

@when('checked lock Filter')
def step_impl(context):
    context.drops_page.checked_lock_Filter()

@when('click on filter_select button')
def step_impl(context):
    context.drops_page.click_on_Filter_Select_Button()

@when('click on drop tile checklist')
@when('click on drop tile checklist {checklist_tile}')
def step_impl(context, checklist_tile="RTL"):
    if (checklist_tile != 'BY-DB'):
        context.db_result.update_db(context.db_con.get_all_details_by_ipconfigureName_and_checklistName(context.navigation_page.Configuration,checklist_tile))
    checklist_tile = context.db_result.db_checklistName
    milestoneID = context.db_result.db_milestoneID
    context.drops_page.clickOnRelevantChecklist(checklist_tile,str(milestoneID))

@when('update drop ip configuration checklist')
def step_impl(context):
    context.drops_page.update_drop_ip_configuration_checklist()

@when('update_checklists_in_drops_page')
def step_impl(context):
    context.drops_page.update_checklists_in_drops_page()


@then("validate ip configuration in the title of drops page")
def step_impl(context):
    context.drops_page.validate_title_of_drops_page(context.navigation_page.Configuration)