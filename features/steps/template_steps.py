import time
from behave import *

# from features_new.steps.comdata_steps import card_add_event_amount, authorized_event_amount, post_with_pre_amount

@step("wait for {sleep_time} seconds")
def step_impl(context, sleep_time):
    time.sleep(int(sleep_time))

@when('template - select search ip configuration and choose checklist')
def step_imp(context):
    context.execute_steps("""
    When user search by "SELECT"
    And click on filter button
    And checked lock Filter
    And click on filter_select button
    And click on drop tile checklist
    """)


