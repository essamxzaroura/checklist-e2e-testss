import time
from behave import given , when , then

from pages_object_model.navigation_page.navigation_page import NavigationPage

@then("validate title website title")
def step_impl(context):
    assert "PEARL Quality" in context.web._web_driver.title , "title is not match it's : %s " %(context.web._web_driver.title)
    context.common_page.click_on_done_button()
    context.web._web_driver.refresh()

@given("user go to search an ip configuration by group pop-up")
def user_on_search_page(context):
    context.navigation_page.click_on_select_different_ip_button()
    context.navigation_page.select_your_ip_by_group()

@given("user search an ip configuration by group pop-up")
@given("user search an ip configuration by group pop-up {Group}:Group {Supplier}:Supplier {Generation}:Generation {Configuration}:ip")
def step_impl(context , Group="CIG-CCD" , Supplier="CCD" , Generation="1GbE 1.0" ,
              Configuration="Nahum10_ADP-S"):
    context.navigation_page.click_on_select_different_ip_button()
    # time.sleep(12) #  work around
    context.navigation_page.select_your_ip_by_group(Group, Supplier, Generation, Configuration)

@given("user search an ip configuration by group pop-up {Configuration}:ip")
def step_impl(context , Configuration):
    if(Configuration == '1'):
        Configuration = context.DB_ipConfigurationName1Con
    elif (Configuration == 'BY-DB'):
        Configuration = context.db_result.db_ipConfigName
    context.navigation_page.click_on_select_different_ip_button()
    context.navigation_page.select_your_ip_by_group(None, None, None, Configuration)

@when('user search by "{selectType}"')
def step_impl(context , selectType="SELECT"):
    if selectType == "SELECT&PIN":
        context.navigation_page.click_on_select_pin_button()
    else:
        context.navigation_page.click_on_select_button()

@when('update_Drops_Recommended')
def step_impl(context):
    context.navigation_page.update_Drops_Recommended()

@when('user go to favorite list')
def step_impl(context):
    context.navigation_page.click_on_select_ip_configuration_button ()

@when("enter Pearl Quality web page")
def step_impl(context):
    context.preferences_window_first_time_user = context.common_page.get_preferences_window_first_time_user().text
    context.common_page.click_on_done_button()

@when("user sets all the preferences checklists to be unchecked")
def step_impl(context):
    Checked_preferences = context.common_page.get_checked_checklists()
    context.common_page.clean_CL_preferences(Checked_preferences)

@then("validate ip configuration in favorite list")
def step_impl(context):
    context.navigation_page.validate_ip_configuration_in_favorite_list(context.navigation_page.Configuration)

@given("user search milestone ID locked: {locked} and mode: {mode}")
@given("user search milestone ID: {milestone_ID}")
@given("user search milestone ID checklistName: {checklistName}")
def step_impl(context, mode='released',milestone_ID=None,locked=0, checklistName=None ):
    if milestone_ID:
        context.navigation_page.search_milestone_ID_textfield(milestone_ID)
    elif checklistName:
        context.db_result.update_db(context.navigation_page.get_milestone_ID_SQL_by_checklistName(context.config.userdata['environment'],checklistName))
        context.navigation_page.search_milestone_ID_textfield(context.db_result.db_milestoneID)
    else:
        context.db_result.update_db(context.navigation_page.get_milestone_ID_SQL_by_mode(context.config.userdata['environment'],locked,mode))
        context.navigation_page.search_milestone_ID_textfield(context.db_result.db_milestoneID)
    time.sleep(0.5)

@given("user search and choose exist milestone ID and checklistname")
def step_impl(context):
    context.navigation_page.search_milestone_ID_textfield(context.db_result.db_milestoneID)
    context.navigation_page.choose_checklist_name_by_search_milestone_ID(context.db_result.db_checklistName)


@when("user update db result")
@when("user update db result locked: {locked}")
def step_impl(context, milestone_ID=None,locked=0):
    context.db_result.update_db(context.navigation_page.get_milestone_ID_SQL_by_mode(context.config.userdata['environment'], locked))

@given("user choose checklist name by search milestone ID")
@given("user choose checklist name : {checklist_name} by search milestone ID")
def step_impl(context, checklist_name=None):
    if checklist_name:
        context.navigation_page.choose_checklist_name_by_search_milestone_ID(checklist_name)
    else:
        context.navigation_page.choose_checklist_name_by_search_milestone_ID(context.db_result.db_checklistName)
    if context.navigation_page.Search_milestone_ID_list_of_version[0]=="Get Started":
        time.sleep(0.5)
        context.common_page.select_version()