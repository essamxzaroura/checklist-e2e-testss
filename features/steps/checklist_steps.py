from behave import given , when , then

from pages_object_model.checklist_page.checklist_page import ChecklistPage
from pages_object_model.navigation_page.navigation_page_locators import NavigationPageLocators
from pages_object_model.drops_page.drops_page_locators import DropsPageLocators
from pages_object_model.checklist_page.checklist_page_locators import ChecklistPageLocators
from pages_object_model.common_page.common_page import CommonPage
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

# @given('user selects the 2 checklists from the DB')
# def step_impl(context):
#     result = {}
#     result = context.checklist_page.select_2_checklists_from_DB_to_copy(context.config.userdata['environment'])
#     for elem in result:
#         if "ChecklistName" in elem:
#             context.DB_checklistNameCon = result.get(elem)
#         if "ipconfigName0" in elem:
#             context.DB_ipConfigurationName1Con = result.get(elem)
#         if "ipconfigName1" in elem:
#             context.DB_ipConfigurationName2Con = result.get(elem)
#         if "milestone0" in elem:
#             context.DB_milestone1Con = result.get(elem)
#         if "milestone1" in elem:
#             context.DB_milestone2Con = result.get(elem)
#         if "DropName" in elem:
#             context.DB_dropNameCon = result.get(elem)

@given('Get the latest checklist information')  
def step_impl(context):
    context.db_result.update_db(context.checklist_page.select_all_checklists_from_DB_app(context.config.userdata['environment']))

@when('user collects the copied rules from checklist "{ipconfigName}" of rule "{index}"')
def step_impl(context,index,ipconfigName):
    context.checklist_page.get_copied_response_rules(ipconfigName,index)

@when('user copies rules "{action}",checklist "{IpConfigurationName}"')
def step_impl(context,action,IpConfigurationName):
    if(IpConfigurationName == '1'):
        context.checklist_page.Copy_Rules(action,context.db_result_2_checklists.db_ipConfigName1,context.db_result_2_checklists.db_drop1,
                                          context.db_result_2_checklists.db_milestoneID1)
    if (IpConfigurationName == '2'):
        context.checklist_page.Copy_Rules(action,context.db_result_2_checklists.db_ipConfigName2,context.db_result_2_checklists.db_drop2,
                                          context.db_result_2_checklists.db_milestoneID2)

@when('user copies requested rules only "{action}",checklist "{IpConfigurationName}"')
def step_impl(context , action , IpConfigurationName):
    time.sleep(1)
    if (IpConfigurationName == '1'):
        context.checklist_page.Copy_Requested_rules_only(action,
        context.db_result_2_checklists.db_ipConfigName1,context.db_result_2_checklists.db_drop1,context.db_result_2_checklists.db_milestoneID1)
    if (IpConfigurationName == '2'):
        context.checklist_page.Copy_Requested_rules_only(action,
        context.db_result_2_checklists.db_ipConfigName2,context.db_result_2_checklists.db_drop2,context.db_result_2_checklists.db_milestoneID2)

@when('return the previous state on checklist "{ipconfigName}" on "{index}" to "{response}"')
def step_impl(context,ipconfigName,index,response):
    time.sleep(2)
    if (ipconfigName =='1' and context.checklist_page.get_ipConfigId_label() != ipconfigName):
        context.checklist_page.open_checklist_by_Ipconfigname_and_checklistIName(context.db_result_2_checklists.db_ipConfigName1, context.db_result_2_checklists.db_checklistName1,
        context.db_result_2_checklists.db_drop1, context.db_result_2_checklists.db_milestoneID1,context.navigation_page, context.drops_page, context.common_page)
    if (ipconfigName =='2'and context.checklist_page.get_ipConfigId_label() != ipconfigName):
        context.checklist_page.open_checklist_by_Ipconfigname_and_checklistIName(context.db_result_2_checklists.db_ipConfigName2, context.db_result_2_checklists.db_checklistName2,
        context.db_result_2_checklists.db_drop2, context.db_result_2_checklists.db_milestoneID2,context.navigation_page, context.drops_page, context.common_page)
    if (ipconfigName == '00'):
        context.checklist_page.open_checklist_by_Ipconfigname_and_checklistIName(context.db_result.db_ipConfigName,context.db_result.db_checklistName,
        context.db_result.db_drop, context.db_result.db_milestoneID,context.navigation_page, context.drops_page, context.common_page)
    context.checklist_page.update_response(response,index)
    context.checklist_page.click_on_submit_button()
    time.sleep(2.5)

@when ('user opens found checklist "{checklistNumber}"')
def step_impl(context,checklistNumber):
    if(checklistNumber == '1'):
        context.checklist_page.open_checklist_by_Ipconfigname_and_checklistIName(context.db_result_2_checklists.db_ipConfigName1,context.db_result_2_checklists.db_checklistName1,
        context.db_result_2_checklists.db_drop1,context.db_result_2_checklists.db_milestoneID1,context.navigation_page,context.drops_page,context.common_page)
    if(checklistNumber == '2'):
        context.checklist_page.open_checklist_by_Ipconfigname_and_checklistIName(context.db_result_2_checklists.db_ipConfigName2, context.db_result_2_checklists.db_checklistName2,
        context.db_result_2_checklists.db_drop2, context.db_result_2_checklists.db_milestoneID2,context.navigation_page, context.drops_page, context.common_page)
    if (checklistNumber == '00'):
        context.checklist_page.open_checklist_by_Ipconfigname_and_checklistIName(context.db_result.db_ipConfigName,context.db_result.db_checklistName,
        context.db_result.db_drop, context.db_result.db_milestoneID,context.navigation_page, context.drops_page, context.common_page)

@when('user updates result to "{response}" in rule "{index}"')
def step_impl(context,response,index):
    context.checklist_page.update_rule(response,index)
    time.sleep(2.5)

@when('user updates rule: "{index}" to "{response}"')
def step_impl(context,index,response):
    if (context.checklist_page.get_ipConfigId_label () != context.db_result.db_ipConfigName):
        context.checklist_page.open_checklist_by_Ipconfigname_and_checklistIName(context.db_result.db_ipConfigName,context.db_result.db_checklistName,
        context.db_result.db_drop,context.db_result.db_milestoneID,context.navigation_page,context.drops_page,context.common_page)
    context.checklist_page.update_response(response,index)
    time.sleep(0.5)
    context.checklist_page.click_on_submit_button()
    context.checklist_page.set_grade_to_actual()
    time.sleep(2)


# @when('user goes to approvers page')
# def step_impl(context):
#     context.common_page.click_on_approvers_button()
