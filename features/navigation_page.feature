Feature: Journey  Tests [Sanity]

  Scenario: 21-Navigation-Search milestone ID by existing One checklist​​​​​​​ [Sanity]
   Given user search milestone ID locked: 0 and mode: released
   Then validate search milestone shows the checklistName and version
   Then validate search milestone shows the revision
   Given user choose checklist name by search milestone ID
   Then validate checklist page shows the details
#
#  Scenario: 20-Save Request[sanity]
#    Given user search milestone ID locked: 0 and mode: released
#    When user opens found checklist "00"
#    When return the previous state on checklist "00" on "0" to "No Response"
#    And return the previous state on checklist "00" on "1" to "No Response"
#    And user updates rule: "0" to "Waiver"
#    And user updates rule: "1" to "NA"
#    Then verify in process request "0"
#    And verify in process request "1"
#    When user sets as approver
#    And user goes to approvers page
#    And user approves request
#    And user approves request
#    And user go to Back
#    Then verify approved request "0"
#    And  verify approved request "1"
#    When return the previous state on checklist "00" on "0" to "No Response"
#    And return the previous state on checklist "00" on "1" to "No Response"
#    And delete the user from approvers
#
#  Scenario: 01-Navigation Page-User search an IP configuration by group [Sanity]
#    Given user search an ip configuration by group pop-up
#    When user search by "SELECT"
#    Then validate ip configuration in the title of drops page
#
#  Scenario: 02-Navigation Page-Pin IP Configuration To Favorites list and Access to IP Configuration[Sanity]
#    Given user search an ip configuration by group pop-up
#    When user search by "SELECT&PIN"
#    And user go to HOME page
#    Then validate ip configuration in favorite list
##
##  Scenario: 03-Navigation Page-Recommended Show 4 Last Visited Checklists and view IP details[Sanity]
##    Given user search an ip configuration by group pop-up 10.0-10.3125GHz LCPLL - 1274.12/.13:ip
##    When template - select search ip configuration and choose checklist
##    And assign version manually to versionName: 2.10.08
##    And user go to HOME page
##    Given user search an ip configuration by group pop-up 2LM-FMHC_ADL:ip
##    When template - select search ip configuration and choose checklist
##    And assign version manually to versionName: 2.10.08
##    And user go to HOME page
##    Given user search an ip configuration by group pop-up 2LM-FMHC_ADL-U:ip
##    When template - select search ip configuration and choose checklist
##    And assign version manually to versionName: 2.10.08
##    And user go to HOME page
##    Given user search an ip configuration by group pop-up ACE_CVF:ip
##    When template - select search ip configuration and choose checklist
##    And assign version manually to versionName: 2.10.08
##    And user go to HOME page
##    Given user search an ip configuration by group pop-up 8-10.0Ghz LCPLL for 1272.5/.6:ip
##    When template - select search ip configuration and choose checklist
##    And assign version manually to versionName: 2.10.08
##    And user go to HOME page
##    And update_Drops_Recommended
##    Then validate 4 Recommended ip configuration checklist
##    And validate Order of 4 Recommended 1.8-10.0Ghz LCPLL for 1272.5/.6 2.ACE_CVF 3.2LM-FMHC_ADL-U 4.2LM-FMHC_ADL
#
#  Scenario: 04-Drops Page-Presented Only 4 Milestones and be clickable[sanity]
#    Given user search an ip configuration by group pop-up
#    When user search by "SELECT"
#    Then validate ip configuration in the title of drops page
#    Then validate only 4 milestones drops page
#
#  Scenario: 05-Drops Page-Presented Only 4 Milestones and be clickable  when SELECT&PIN
#    Given user search an ip configuration by group pop-up
#    When user search by "SELECT&PIN"
#    Then validate ip configuration in the title of drops page
#    Then validate only 4 milestones drops page
#
#  @FirstTimeUser
#  Scenario: 06-First time user - presented a single question [sanity]
#     When enter Pearl Quality web page
#     Then validate the checklists preferences window opens
#
#  @FirstTimeUser
#  Scenario: 07-First time user - all checked CL are saved
#    When user sets all the preferences checklists to be checked
#    Then validate all the checklists are checked
#
## unit test
##  @FirstTimeUser
##  Scenario: 08-First time user - is disabled Done button when unchecked all Options
##    When user sets all the preferences checklists to be unchecked
##    Then validate the done button is disabled
#
#  @FirstTimeUser
#  Scenario: 08-First time user - Part of checked CL are saved
#    When user randomly sets the preferences checklists to be checked
#    Then validate setted preferences checklists are checked
#
#  Scenario: 09-Preferences - changes in checked CL are saved[Sanity]
#    Given user search an ip configuration by group pop-up
#    When user search by "SELECT"
#    And click on filter button
#    And checked lock Filter
#    And click on filter_select button
#    And update drop ip configuration checklist
#    Given click on the prefernces icon
#    When prefernces uncheck checklist type
#    And prefernces click save button
#    And update drop ip configuration checklist
#    Then validate checklist is not present type
#
#  @Preferences
#  Scenario: 10-Preferences - all checked CL are saved
#    When user sets all the preferences checklists to be checked
#    Then validate all the checklists are checked
#
#  @Preferences
#  Scenario: 11-Preferences updated in Different IP configuration
#    Given user search an ip configuration by group pop-up Tremont_JSP:ip
#    When user search by "SELECT"
#    And click on filter button
#    And checked lock Filter
#    And click on filter_select button
#    And update drop ip configuration checklist
#    And user go to HOME page
#    Given user search an ip configuration by group pop-up Gracemont 1.0:ip
#    When user search by "SELECT"
#    And click on filter button
#    And checked lock Filter
#    And click on filter_select button
#    Given click on the prefernces icon
#    When prefernces uncheck checklist type
#    And prefernces click save button
#    And update drop ip configuration checklist
#    Then validate checklist is not present type
#    When user go to HOME page
#    Given user search an ip configuration by group pop-up PXP PCIex8_RK:ip
#    When user search by "SELECT"
#    And click on filter button
#    And checked lock Filter
#    And click on filter_select button
#    Then validate checklist is not present type
#
#  @Preferences
#  Scenario: 12-Preferences Part of checked CL are saved
#    Given click on the prefernces icon
#    When prefernces uncheck checklist type HAS
#    And prefernces uncheck checklist type RTL
#    And prefernces click save button
#    Given user search an ip configuration by group pop-up
#    When user search by "SELECT"
#    And click on filter button
#    And checked lock Filter
#    And click on filter_select button
#    And update drop ip configuration checklist
#    Given click on the prefernces icon
#    When prefernces check checklist type HAS
#    And prefernces check checklist type RTL
#    And prefernces click save button
#    And update drop ip configuration checklist
#    Then validate checklist is not present type HAS
#    And validate checklist is not present type RTL
#
#  @Copy_From_To
#  Scenario: 13-Copy_From_[Sanity]
#    When user opens found checklist "2"
#    And assign version manually to versionName: 2.10.08
#    And user updates result to "No Response" in rule "0"
#    And user updates result to "No Response" in rule "1"
#    And user updates result to "Passed" in rule "0"
#    And user collects the copied rules from checklist "2" of rule "0"
#    And user go to HOME page
#    And user opens found checklist "1"
#    And user updates result to "No Response" in rule "0"
#    And user updates result to "No Response" in rule "1"
#    And user copies rules "from",checklist "2"
#    And user collects the copied rules from checklist "1" of rule "0"
#    Then validate copied rules results
#    When return the previous state on checklist "1" on "0" to "No Response"
#    And return the previous state on checklist "2" on "0" to "No Response"
#
#  @Copy_From_To
#  Scenario: 14-Copy_To_[Sanity]
#    When user opens found checklist "1"
#    And assign version manually to versionName: 2.10.08
#    And user updates result to "No Response" in rule "0"
#    And user go to HOME page
#    And user opens found checklist "2"
#    And assign version manually to versionName: 2.10.08
#    And user updates result to "No Response" in rule "0"
#    And user updates result to "Passed" in rule "0"
#    And user collects the copied rules from checklist "2" of rule "0"
#    And user copies rules "to",checklist "1"
#    And user opens found checklist "1"
#    And user collects the copied rules from checklist "1" of rule "0"
#    Then validate copied rules results
#    When return the previous state on checklist "1" on "0" to "No Response"
#    And return the previous state on checklist "2" on "0" to "No Response"
##
###  @Copy_From_To
###  Scenario: 15-Copy From Copy ALL Approval Request Rules[sanity]
###    When user opens found checklist "2"
###    And assign version manually to versionName: 2.10.08
###    And user updates result to "Waiver" in rule "0"
###    And user updates result to "Passed" in rule "1"
###    And user go to HOME page
###    And user opens found checklist "1"
###    And assign version manually to versionName: 2.10.08
###    And user copies requested rules only "from",checklist "2"
###    And user collects the copied rules from checklist "1" of rule "0"
###    And user collects the copied rules from checklist "1" of rule "1"
###    Then validate copied rules with approval requests only
###    When return the previous state on checklist "2" on "0" to "No Response"
###    And return the previous state on checklist "2" on "1" to "No Response"
###    And return the previous state on checklist "1" on "1" to "No Response"
###
###  @Copy_From_To
###  Scenario: 16-Copy To Copy ALL Approval Request Rules[sanity]
###    When user opens found checklist "2"
###    And assign version manually to versionName: 2.10.08
###    And user updates result to "Waiver" in rule "1"
###    And user updates result to "Passed" in rule "0"
###    And user sets as approver "2"
###    And user goes to approvers page
###    And user approves request
###    And user go to Back
###    And user copies requested rules only "to",checklist "1"
###    And user collects the copied rules from checklist "1" of rule "0"
###    And user collects the copied rules from checklist "1" of rule "1"
###    Then validate copied rules with approval requests only
###    When return the previous state on checklist "1" on "0" to "No Response"
###    And return the previous state on checklist "1" on "1" to "No Response"
###    And return the previous state on checklist "2" on "0" to "No Response"
###    And return the previous state on checklist "2" on "1" to "No Response"
#
#  Scenario: 17-Update and present Grade[sanity]
#    Given user search milestone ID locked: 0 and mode: released
#    When user opens found checklist "00"
#    When return the previous state on checklist "00" on "3" to "No Response"
#    And return the previous state on checklist "00" on "4" to "No Response"
#    And user updates result to "Passed" in rule "3"
#    And user updates result to "Passed" in rule "4"
#    Then validate the grade was changed
#    When return the previous state on checklist "00" on "3" to "No Response"
#    And return the previous state on checklist "00" on "4" to "No Response"
#
#  Scenario: 18-Present Rules Details[sanity]
#    Given user search milestone ID locked: 0 and mode: released
#    When user opens found checklist "00"
#    Then validate the rules are presented
#
#  Scenario: 19-Owned By Me Show IP Configurations[Sanity]
#    When user update db result locked: 0
#    When user makes himself an owner of IP configuration
#    Then validate IPs Owned By Me
#    When delete owner "1"
#     #Need to move this step in the after
#
# Scenario: 22-Navigation-Search milestone - Wrong Millstone Id Number [Negative]
#   Given user search milestone ID: 00000
#   Then validate search milestone throw error not found
#
##    Because bug this step in comment
## Scenario: 23-Navigation-Search milestone - Wrong Millstone Id String [Negative]
##   Given user search milestone ID: Essam
##   Then validate search milestone throw error not found
#
#  Scenario: 24-Navigation-Search milestone - Show 1st revision in the title of the search
#   Given user search milestone ID locked: 0 and mode: released
#   Then validate search milestone shows the revision
#
# Scenario: 25-Navigation-Search milestone - Search for locked checklist
#   Given user search milestone ID locked: 1 and mode: released
#   Then validate search milestone shows the checklistName and version
#   Given user choose checklist name by search milestone ID
#   When user update db result locked: 1
#   Then validate checklist page shows the details
#
##  Bug
## Scenario: 26-Navigation-Search milestone - unchecked checklist Type should not appears in the search
##   Given user search milestone ID checklistName: RTL
##   And click on the prefernces icon
##   When prefernces uncheck checklist type RTL
##   And prefernces click save button
##   And user go to HOME page
##   Given user search milestone ID checklistName: RTL
##   Then validate Navigation page is not presented checklistName RTL in search milestone
#
# Scenario: 27-Version-Assigned version-Assigned it not by default version in the recommended&Drops page [Sanity]
#   Given db-get checklist by version: Get Started
#   And user search an ip configuration by group pop-up BY-DB:ip
#   When user search by "SELECT"
#   And click on filter button
#   And checked lock Filter
#   And click on filter_select button
#   And click on drop tile checklist BY-DB
#   And assign version manually to versionName: 2.10.08
#   And user go to Back
#   And update_checklists_in_drops_page
#   Then validate in drops page checklistName BY-DB is versionName: 2.10.08
#
# Scenario: 28 - General - Development/Released - released requests is not found in Development mode[Sanity]
#   When switch mode to: development
#   Given user search milestone ID locked: 0 and mode: development
#   And user choose checklist name by search milestone ID
#   When return the previous state on checklist "00" on "0" to "No Response"
#   And user updates rule: "0" to "Waiver"
#   Then verify in process request "0"
#   When wait for 2 seconds
#   When user go to HOME page
#   And switch mode to: released
#   Given user search and choose exist milestone ID and checklistname
#   Then validate the rule 0 is "No Response"
#   When user sets as approver
#   And user goes to approvers page
#   Then validate approval request is not found
