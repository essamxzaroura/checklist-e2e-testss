
import shutil
import time

from pages_object_model.common_page.common_page import CommonPage
from pages_object_model.drops_page.drops_page import DropsPage
from pages_object_model.navigation_page.navigation_page import NavigationPage
from pages_object_model.checklist_page.checklist_page import ChecklistPage
from pages_object_model.base_page.base_page_web import BasePageWeb
from pages_object_model.approvers_page.approvers_page import ApproversPage
from web_source.web_factory import get_web
from utils.allure_utils import attach_png
from utils import connectToDB
from utils import db_result

import os


def before_all(context):
    # clear_results()
    # context.hooks.before_scenario()
    context.web=get_web(context.config.userdata['browser'])
    context.db_con = connectToDB.SQLQuiries(context.config.userdata['environment'])
    context.db_con.save_cl_preferences_user_from_tblUserSettings(context.config.userdata['user'])
    print("Running on Env: %s ,with user: %s " %(context.config.userdata['environment'], context.config.userdata['user']))
    if context.config.userdata['environment']=='DEV':
        context.web.open("https://ipquality-dev.swiss.intel.com")
        print("Running on DEV")
    else:
        context.web.open("https://ipquality-qa.swiss.intel.com/")
        # context.web.open("https://pearl-quality-qa.app.intel.com/")
        # context.web.open("https://sys_pearl3writer:pearl123!@pearl-quality-qa.app.intel.com/")
    context.common_page = CommonPage( context.web._web_driver )
    context.navigation_page = NavigationPage(context.web._web_driver)
    context.drops_page = DropsPage(context.web._web_driver)
    context.checklist_page = ChecklistPage(context.web._web_driver)
    context.base_page = BasePageWeb(context.web._web_driver)
    context.approvers_page = ApproversPage(context.web._web_driver)
    context.init_db_result_2_checklists = True
    context.DB_checklistNameCon = ""
    context.DB_ipConfigurationName1Con = ""
    context.DB_ipConfigurationName2Con = ""
    context.DB_dropNameCon = ""
    context.DB_ownerCon = ""
    # context.web._web_driver.save_screenshot("before_all_02_second.png")

# def before_step(context,step="Step Name"):
#     attach_png((context.web._web_driver).get_screenshot_as_png(),step)
def after_step(context,step="Step Name"):
    attach_png((context.web._web_driver).get_screenshot_as_png(),step)

def before_scenario(context,scenario):
    if("Copy_From_To" not in scenario.tags):
        context.db_result = db_result.DB_Resutlt()
    if("FirstTimeUser" not in scenario.tags) and (context.common_page.get_If_first_time_user()):
        context.common_page.click_on_done_button()
        context.web._web_driver.refresh()

def before_tag(context, tag):
    if(tag == 'FirstTimeUser'):
        context.db_con.delete_user_from_tblUserSettings(context.config.userdata['user'])
        time.sleep(1)
        context.web._web_driver.refresh()
    if (tag == 'Preferences' or tag == 'Checklists'):
        context.common_page.click_on_settings_button()
        context.common_page.AllCheckboxesAreFull()
    # time.sleep(context.config.userdata['delay'])
    if (tag=='Copy_From_To' and context.init_db_result_2_checklists):
        context.db_result_2_checklists = db_result.DB_Resutlt_2_checklists()
        context.init_db_result_2_checklists = False
        context.db_result_2_checklists.update_2_checklists(context.db_con.copy_from_to_get_2_relevant_checklists())
    attach_png(context.web._web_driver.get_screenshot_as_png(), "Imagex")
    context.db_result = None

def after_scenario(context, scenario):
    if context.scenario.status == 'failed':
        attach_png(context.web._web_driver.get_screenshot_as_png(), "Image")
    # context.web._web_driver.save_screenshot ( "after_scenario.png" )
    context.common_page.refresh()# Work around need to deleted because bug DE48229
    context.common_page.click_on_HOME__button()
    if("Copy_From_To" not in scenario.tags):
        context.common_page.db_return_Get_started(context.config.userdata['environment'],
                                                  context.db_result.db_milestoneID, context.db_result.db_checklistID)

def after_all(context):
    print("after_all_tests")
    context.web.close()
    context.web.quit()

def clear_results():
    print("remove tests-results/allure-behave folder")
    if os.path.isdir('allure-behave'):
        shutil.rmtree('allure-behave')
    return "Done"