from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
import time

class Web(object):
    __TIMEOUT = 10

    def __init__(self, web_driver):
        super(Web, self).__init__()  # in python 3.6 you can just call super().__init__()
        self._web_driver_wait = WebDriverWait(web_driver, Web.__TIMEOUT)
        self._web_driver = web_driver

    def open(self, url):
        self._web_driver.implicitly_wait(5)
        self._web_driver.maximize_window()
        self._web_driver.get(url)

    def close(self):
        self._web_driver.close()
        print("Test completed - close browsre ")
        # display.stop()

    def quit(self):
        self._web_driver.quit()
        print("Test completed - quit browsre ")

    def find_by_xpath(self, xpath):
        return self._web_driver_wait.until(EC.visibility_of_element_located((By.XPATH, xpath)))

    def finds_by_xpath(self, xpath):
        return self._web_driver_wait.until(EC.presence_of_all_elements_located((By.XPATH, xpath)))

    #// All function in base page web
    def go_back(self):
        self._web_driver.back()

    def find_element(self, *locator):
        return self._web_driver.find_element(*locator)

    # def open(self,url):
    #     url = self.base_url + url
    #     self.driver.get(url)

    def get_title(self):
        return self._web_driver.title

    def get_url(self):
        return self._web_driver.current_url

    def hover(self, *locator):
        element = self.find_element(*locator)
        hover = ActionChains(self._web_driver).move_to_element(element)
        hover.perform()

    def refresh(self):
        time.sleep(1)
        self._web_driver.refresh()
        time.sleep(2)
        # add pearl-cookie

    def wait_and_click_on_element(self, *locator):
        try:
           element = WebDriverWait(self._web_driver, 20).until(EC.element_to_be_clickable((locator)))
           element.click()
        finally:
            return True

    def wait_to_be_displayed(self,*locator):
        try:
            element = WebDriverWait(self._web_driver, 20).until(EC.presence_of_all_elements_located(locator))
            return True
        except:
            return False

    def wait_for_element_be_invisible(self, *locator):
        try:
            element = WebDriverWait(self._web_driver, 20).until(EC.invisibility_of_element_located(locator))
            return True
        except:
            return False