from selenium import webdriver
from web_source.web import Web
from selenium.webdriver.chrome.options import Options
import selenium.webdriver.chrome.service as service
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import os.path


def get_web(browser):
    if browser == "chrome":
        options = Options()
        options.binary_location = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"
        options.add_argument('--ignore-certificate-errors')
        # options.add_argument("--start-maximized")
        # options.add_argument("user-data-dir=C:\\Users\\sys_pearl3writer\\AppData\\Local\\Google\\Chrome\\User Data\\Profile 2")
        # options.add_argument("--headless") # Runs Chrome in headless mode.
        return Web(webdriver.Chrome(chrome_options=options,executable_path=os.path.join(os.getcwd(),"bin","chromedriver.exe")))
        # return Web(webdriver.Chrome(executable_path=r'C:\\Projects-VSCode\\APT2\\checklist-e2e-tests\\bin\\chromedriver.exe',chrome_options=options))
                                           