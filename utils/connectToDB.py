import pyodbc
import sys


class SQLQuiries(object):
    server = 'sql1070-lc-in.ger.corp.intel.com,3181'
    database = 'Quality_QA'
    uid = 'Quality_QA_so'
    pwd = 'oDzPm171qHa0bJs'
    def __init__(self,env):#take out the database, password,username as variables
        if env == 'DEV':
            try:
                # DB on DEV
                self.conn = pyodbc.connect(
                    'DRIVER={SQL Server};SERVER=sql1070-lc-in.ger.corp.intel.com,3181;DATABASE=Quality_DEV;UID=Quality_DEV_so;PWD=cHcAhJaWk7dClWp')
                self.cursor = self.conn.cursor()
            except pyodbc.Error:
                print("Error has occured in init DEV db connection")
                sys.exit(1)
        else:
            try:
                # QA
                self.conn = pyodbc.connect('DRIVER={SQL Server};SERVER=sql1070-lc-in.ger.corp.intel.com,3181;DATABASE=Quality_QA;UID=Quality_QA_so;PWD=oDzPm171qHa0bJs')
                self.cursor = self.conn.cursor ()
            except pyodbc.Error:
                print ("Error has occured in init QA db connection")
                sys.exit(1)

    def delete_user_from_tblUserSettings(self,idsid):
        self.cursor.execute('delete  from [dbo].[tblUserSettings] where idsid = (?)', (idsid))
        self.conn.commit()

    def save_cl_preferences_user_from_tblUserSettings(self,idsid):
        self.delete_user_from_tblUserSettings(idsid)
        # self.cursor.execute('insert into [dbo].[tblUserSettings] values ((?),1,Null),((?),2,Null),((?),3,Null),((?),4,Null),((?),7,Null),((?),8,Null),((?),9,Null)',(idsid),(idsid),(idsid),(idsid),(idsid),(idsid),(idsid))
        self.cursor.execute('insert into [dbo].[tblUserSettings] select (?), checklistID,Null from [dbo].[tblChecklistDef]', (idsid))
        self.conn.commit ()


    def select_all_checklists_from_vwChecklistStatus(self):
        self.cursor.execute('select TOP 1 checklistid as checklistID,checklistName as checklistName, [drop] as dropName, milestoneid as milestoneID, ipconfigName as ipconfigName, ipConfigId as id,ipfamily,isLocked,versionName,versionID,revision '
                            'from [dbo].[vwChecklistStatus] join tblDrops D on [drop] = D.[dropname]'
                            'where [DropType] is not null and islocked = (?) and [order] < 5 and [numberOfPassed] is null and completeDate is null and [versionName] is not null' , (0) )
        result = self.cursor.fetchall()
        self.conn.commit()
        return result

    def copy_from_return_2_lists(self):
        self.cursor.execute('select distinct TOP 1 FirstIP.checklistID as checklistidTo,FirstIP.checklistName as checklistNameTo,FirstIP.[drop] as dropTo,FirstIP.milestoneID as milestoneidTo,FirstIP.ipConfigName as nameTo,FirstIP.ipConfigID as idTo,SecondIP.checklistID as checklistidFrom,SecondIP.checklistName as checklistNameFrom,SecondIP.[drop] as dropFrom,SecondIP.milestoneID as milestoneidFrom,SecondIP.ipConfigName as nameFrom,SecondIP.ipConfigID as idFrom from [dbo].[vwChecklistStatus] FirstIP join  [dbo].[vwChecklistStatus]  SecondIP on FirstIP.ipConfigID <> SecondIP.ipConfigID and FirstIP.checklistid = SecondIP.checklistid and FirstIP.ipFamily = SecondIP.ipFamily and FirstIP.[Drop] = SecondIP.[Drop] where FirstIP.isLocked = 0 and FirstIP.[numberOfPassed] is null and  SecondIP.isLocked = 0 and SecondIP.[numberOfPassed] is null')
        result = self.cursor.fetchall()
        self.conn.commit()
        return result

    def copy_from_to_get_relevant_data_for_checklist(self):
        self.cursor.execute('select TOP 1 ipfamily as ipFamily,checklistid as checklistID,[drop] as DropName '
                            'from vwChecklistStatus V join tblDrops D on V.[drop] = D.[dropname]  '
                            'where [DropType] is not null and islocked = 0 and [numberOfPassed] is null and completeDate is null and [versionName] is not null and [order] < 5')
        result = self.cursor.fetchall()
        self.conn.commit()
        return result

    def get_relevant_checklists_to_copy_from_to(self,ipfamily,checklistid,dropname):
        self.cursor.execute('select TOP 2 checklistName as ChecklistName,[drop] as DropName,ipconfigName as ipconfigName,milestoneid as milestone from vwChecklistStatus where ipfamily = (?)  and checklistid = (?)  and [drop] = (?) and islocked = 0 and [numberOfPassed] is null ',ipfamily,checklistid,dropname)
        result = self.cursor.fetchall()
        self.conn.commit()
        return result

    def get_owner_by_ipConfigID(self,ipConfigID):
        self.cursor.execute('select Top 1 idsid as IDowner from [dbo].[tblDefaultOwners] where ipconfigid = (?)', ipConfigID)
        result = self.cursor.fetchall()
        self.conn.commit()
        return result


    def update_owner_by_ipConfigID(self,username,ipConfigID):
        self.cursor.execute('update [dbo].[tblDefaultOwners] set idsid = (?) where ipconfigid=(?)',username,ipConfigID)
        self.conn.commit()

    def make_owner_by_ipConfigID(self,ipConfigID,username):
        self.cursor.execute('insert into [dbo].[tblDefaultOwners] Values ((?),1,(?))',ipConfigID,username)
        self.conn.commit()

    def delete_owner_by_ipConfigID_and_idsid(self,username,ipConfigID):
        self.cursor.execute('delete from [dbo].[tblDefaultOwners] where idsid = (?) and ipconfigid = (?)',username,ipConfigID)
        self.conn.commit()

    def delete_assigned_checklist_version_get_started(self,milestoneID,checklistID):
        self.cursor.execute('delete from [tblAssignedChecklistVersionPROD] where milestoneid = (?) and checklistID = (?)',milestoneID,checklistID)
        self.conn.commit()

    def get_milestone_id_by_locked(self, locked=0):
        if locked==0:
            self.cursor.execute(
                'select top 1 checklistID,checklistName,[drop],milestoneID,ipConfigName,ipConfigID,ipfamily,isLocked,versionName,versionID,revision '
                'from [dbo].[vwChecklistStatus] where islocked = (?) and checklistName<> (?)',locked,'Zircon')
        else:
            self.cursor.execute(
                'select top 1 checklistID,checklistName,[drop],milestoneID,ipConfigName,ipConfigID,ipfamily,isLocked,versionName,versionID,revision '
                'from [dbo].[vwChecklistStatus] where islocked = (?) and checklistName<> (?)',locked,'Zircon')
        result = self.cursor.fetchall()
        self.conn.commit()
        return result

    def get_milestone_id_by_locked_and_mode(self, locked=0,mode='released'):
        mode = '' if mode == 'released' else 'QA'
        # self.cursor.execute('select top 1 checklistID,checklistName,[drop],milestoneID,ipConfigName,ipConfigID,ipfamily,isLocked,versionName,versionID,revision '
        #                     'from [dbo].[vwChecklistStatus%s] where islocked = (?) and checklistID != 10'%mode,locked)
        self.cursor.execute('select top 1 checklistID,checklistName,[drop],milestoneID,ipConfigName,ipConfigID,ipfamily,isLocked,versionName,versionID,revision '
                            'from [dbo].[vwChecklistStatus%s] where islocked = (?)'%mode,locked)
        result = self.cursor.fetchall()
        self.conn.commit()
        return result

    def get_checklist_id_by_checklistName_and_version(self,versionName, checklistName):
        if (versionName=='Get Started')and (checklistName):
            self.cursor.execute('select top 1 checklistID,checklistName,[drop],milestoneID,ipConfigName,ipConfigID,ipfamily,isLocked,versionName,versionID,revision '
                                'from [dbo].[vwChecklistStatus] where[numberOfRules] is null and checklistName = (?)',checklistName)
        elif (versionName=='Get Started')and not(checklistName):
            self.cursor.execute('select top 1 checklistID,checklistName,[drop],milestoneID,ipConfigName,ipConfigID,ipfamily,isLocked,versionName,versionID,revision '
                                'from [dbo].[vwChecklistStatus] where[numberOfRules] is null ')
        elif not(versionName=='Get Started')and not(checklistName):
            self.cursor.execute('select top 1 checklistID,checklistName,[drop],milestoneID,ipConfigName,ipConfigID,ipfamily,isLocked,versionName,versionID,revision '
                                'from [dbo].[vwChecklistStatus] where versionName = (?)', versionName)
        else:
            self.cursor.execute('select top 1 checklistID,checklistName,[drop],milestoneID,ipConfigName,ipConfigID,ipfamily,isLocked,versionName,versionID,revision '
                                'from [dbo].[vwChecklistStatus] where checklistName = (?) and versionName = (?)', checklistName, versionName)
        result = self.cursor.fetchall()
        self.conn.commit()
        return result

    def get_approver_by_ipconfigName(self , ipconfigname):
        self.cursor.execute('select idsid from [dbo].[tblApproversBySupplier] where supplier = (select supplier from [dbo].[V_DSS_IPCONFIGURATION] where ipconfigname =(?))',ipconfigname)
        result = self.cursor.fetchall()
        self.conn.commit()
        return result

    def sets_as_approver_by_ipconfigName(self,username, ipconfigname):
        self.cursor.execute(' INSERT INTO [dbo].[tblApproversBySupplier] SELECT DISTINCT (select supplier from [dbo].[V_DSS_IPCONFIGURATION]'
                            'where ipconfigname = (?)),[groupID],3,(?) FROM [dbo].[tblChecklistPermissionGroups]',ipconfigname ,username)
        self.conn.commit()

    def make_approver_by_ipConfigName(self , ipconfigName , username):
        self.cursor.execute('select supplier from [dbo].[V_DSS_IPCONFIGURATION] where ipconfigname =(?)',ipconfigName)
        supplier = self.cursor.fetchall()
        self.conn.commit()
        self.cursor.execute('insert into[dbo].[tblApproversBySupplier] VALUES ((?),1,3,(?))',supplier[0][0],username)
        self.conn.commit()

    def delete_approver_by_ipConfigName_and_idsid(self, username, ipconfigName):
        self.cursor.execute('delete from [dbo].[tblApproversBySupplier] where idsid = (?) and supplier = (select supplier from [dbo].[V_DSS_IPCONFIGURATION] where ipconfigname =(?))',username,ipconfigName)
        self.conn.commit()

    def get_all_details_by_ipconfigureName_and_checklistName(self, ipconfigName, checklistName):
        self.cursor.execute('select top 1 checklistID,checklistName,[drop],milestoneID,ipConfigName,ipConfigID,ipfamily,isLocked,versionName,versionID,revision '
                            'from [dbo].[vwChecklistStatus] where checklistName = (?) and ipconfigName = (?)', checklistName,ipconfigName)
        result = self.cursor.fetchall()
        self.conn.commit()
        return result

    def copy_from_to_get_2_relevant_checklists(self):
        RTL='RTL1P0'
        self.cursor.execute('select TOP 2 checklistName as ChecklistName,[drop] as DropName,ipconfigName as ipconfigName,milestoneid as milestone from vwChecklistStatus as vw '
                            'join (select TOP 1 ipfamily as ipFamily,checklistid as checklistID,[drop] as DropName from vwChecklistStatus V join tblDrops D on V.[drop] = D.[dropname] '
                            'where islocked = 0 and [drop] = (?) and [numberOfPassed] is null group by ipfamily,checklistid,[drop] having count (distinct milestoneid) > 1 ) as family '
                            'on vw.ipFamily = family.ipFamily and vw.checklistID = family.checklistID and vw.[drop] = family.[dropname] '
                            'where islocked = 0 and [numberOfPassed] is null',RTL)
        result = self.cursor.fetchall()
        self.conn.commit()
        return result