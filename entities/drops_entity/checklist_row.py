

class Checklist_Row():
# in the recomended and drop page
    def __init__(self, list):
        if len(list)==2:
            self.update_get_started_checklist(list)
        else:
            self.update_checklist_row(list)

    def update_get_started_checklist(self,list):
        self.ui_checklistName = list[0]
        self.ui_versionName = list[1]

    def update_checklist_row(self,list):
        self.ui_checklistName = list[0]
        self.ui_grade = list[1]
        self.ui_versionName = list[2]
        self.ui_needs_response_name = list[3]
        self.ui_needs_response_value = list[4]
        self.ui_pending_requests_name = list[5]
        self.ui_pending_requests_value = list[6]
        self.ui_rejected_requests_name = list[7]
        self.ui_rejected_requests_value = list[8]
        if len(list) == 9:
            self.ui_lock = 'unlock'
        else:
            self.ui_lock = list[9]