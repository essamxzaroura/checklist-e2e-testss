from entities.drops_entity.checklist_row import Checklist_Row
from pages_object_model.drops_page.drops_page_locators import DropsPageLocators


class DropChecklists():
    def __init__(self, drppt_type=None, revision=None, tren_ww=None, milestone_id=None, drop_tile_checklist=None, versionName=None):
        self.drppt_type = drppt_type
        self.tren_ww = tren_ww
        self.revision = revision
        self.milestone_id = milestone_id
        #need to adjust it to many checklist row per drop
        self.drop_tile_checklist = drop_tile_checklist
        self.versionName = versionName


    #     self.checklist_rows=[]
    #
    # def update_Checklist_row(self,self_driver):
    #     drops_checklist_row_locator=self_driver.find_elements(*DropsPageLocators.Drop_Checklist_Row)
    #     self.dropChecklistsRowList = []
    #     for drop in drops_checklist_row_locator:
    #         self.dropChecklistsRowList.append(Checklist_Row(checklist_name=drop.text))



